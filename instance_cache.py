# -*- coding: utf-8 -*-
import threading
import time
import logging
import pdb #FIXME

_lock = threading.RLock()
_cache = {}
_debug = False # or "key", "all"

def _debug_info(*args, **kwargs):
    global _cache
    if _debug is False:
        return
    logging.info(*args, **kwargs)
    for k, v in _cache.items():
        if _debug == 'all':
            logging.info('[%s][%r]' % (k, v))
        else:
            logging.info('[%s]' % k)
            if isinstance(v, dict):
                for sk in v:
                    logging.info('[%s][%s]' % (k, sk))

def cache(available_seconds=600, category=None):
    def decorator(target):
        key = "%s.%s" % (target.__module__, target.__name__)

        def wrapper(*args, **kwargs):
            return request_cache_check_set_return(
                target,
                lambda *a, **kw: key,
                available_seconds=available_seconds,
                category=category,
                *args, **kwargs)

        return wrapper
    return decorator

def cache_with_key_fxn(key_fxn, *, available_seconds=600, category=None):
    def decorator(target):
        def wrapper(*args, **kwargs):
            return request_cache_check_set_return(
                target,
                key_fxn,
                available_seconds,
                category,
                *args,
                **kwargs)

        return wrapper
    return decorator

def request_cache_check_set_return(
        target,
        key_fxn,
        available_seconds,
        category,
        *args,
        **kwargs):
    skip_cache = False
    if "skip_cache" in kwargs:
        skip_cache = kwargs["skip_cache"]
        # delete from kwargs so it's not passed to the target
        del kwargs["skip_cache"]
    if skip_cache:
        # return without get/set cache
        return target(*args, **kwargs)

    key = key_fxn(*args, **kwargs)

    result = get_cache(category, key)
    if result:
        return result

    result = target(*args, **kwargs)
    set_cache(category, key, result, available_seconds)
    return result


def set_cache(category, key, value, available_seconds):
    now = int(time.time())
    if available_seconds > 0:
        exp = now + available_seconds
    else:
        exp = 0
    _debug_info('set [%r][%r]' % (category, key))
    with _lock:
        if category:
            if category not in _cache:
                _cache[category] = {}
            _cache[category][key] = (value, exp)
        else:
            _cache[key] = (value, exp)
    _debug_info('set ok [%r][%r]' % (category, key))
    return


def get_cache(category, key):
    now = int(time.time())
    value = None
    expire_time = -1
    with _lock:
        if category:
            if category in _cache and key in _cache[category]:
                (value, expire_time) = _cache[category][key]
        else:
            if key in _cache:
                (value, expire_time) = _cache[key]
    if value:
        if expire_time == 0 or now < expire_time:
            _debug_info('hit [%s]' % key)
            return value
        _debug_info('hit but expired [%s]' % key)
    return None

def flush(category=None):
    global _cache
    _debug_info('flush [%r]' % category)
    with _lock:
        if category:
            if category in _cache:
                _cache[category] = {}
        else:
            _cache = {}
    _debug_info('flushed [%r]' % category)
