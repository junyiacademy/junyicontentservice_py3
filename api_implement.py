# -*- coding: utf-8 -*-
import auth.user_util
import content.exercise
import content.section
import content.topic
import content.video
import instance_cache
from sheet.sheet import get_content_external_change


def get_topic_page_json_impl(topic_id, version_id=None):
    current_user = auth.user_util.User.current()
    include_hidden = current_user.is_moderator if current_user else False
    if version_id == 'edit':
        # 目前僅先開放 edit version，除此之外都抓 default version
        return content.topic.get_topic_page(topic_id=topic_id, include_hidden=include_hidden,
                                            version_id=version_id, current_user=current_user)
    else:
        return content.topic.get_topic_page(topic_id=topic_id, include_hidden=include_hidden)


def get_topic_page_data(topic_id, version_id=None):
    current_user = auth.user_util.User.current()
    include_hidden = current_user.is_moderator if current_user else False
    if version_id == 'edit':
        # 目前僅先開放 edit version，除此之外都抓 default version
        # pylint: disable=E1123
        return content.topic.get_topic_page_data(topic_id=topic_id, include_hidden=include_hidden,
                                            version_id=version_id, current_user=current_user)
    else:
        return content.topic.get_topic_page_data(topic_id=topic_id, include_hidden=include_hidden)


def get_topic_list_impl(version_id):
    return content.topic.get_exercise_topics(version_id)


def validate_or_update_topic_by_edu_sheet(topic_id, title, description, validation_only, version_id="edit"):
    if version_id != "edit":
        return {
            'topic_id': topic_id,
            'errors': ['目前禁止於 edit 以外的版本修改 Topic']
        }

    err_response, updated_topic = content.topic.update_topic(topic_id, title, description, validation_only, version_id)
    return {
        'topic_id': topic_id,
        'errors': err_response
    }


def validate_or_create_topic_by_edu_sheet(topic_id, title, description, parent_id, validation_only, version_id="edit"):
    """
    將 topic 和 parent_topic 的驗證搬到 validation_only 之後，
    避免教育組正常的上架流程被「檢查變動」擋住。
    e.g. 想要批次建立以下的樹
         A      一開始 A, B, C 都不存在，如果在 validation_only 就檢查，
         |-B    會發現 B 的 parent A 不存在而失敗。因此改到真的要建立時才
           |-C  檢查，就會先建出 A，再來建立 B 的時候 A 就存在了。
    """
    if version_id != "edit":
        return {
            'topic_id': topic_id,
            'errors': ['目前禁止於 edit 以外的版本建立 Topic']
        }

    errors = content.topic.validate_topic_required_info_by_edu_sheet(topic_id, title, version_id)
    if not errors and not validation_only:
        errors, created_topic = content.topic.create_topic(topic_id, title, description, parent_id, version_id)
    return {
        'topic_id': topic_id,
        'errors': errors
    }


def create_topic(topic_id, title, description, parent_id, version_id="edit"):
    errors, created_topic = content.topic.create_topic(topic_id, title, description, parent_id, version_id)
    return {
        'topic_id': created_topic.get('id') if created_topic else topic_id,
        'errors': errors
    }


def exercise_get_all_name(live=True):
    return content.exercise.exercise_get_all_name(
        live_only=live,
        include_exam=False
    )


def validate_or_create_exercise_by_edu_sheet(
        exercise_name, content_title, description, parent_id, grade, validation_only):
    err_response = content.exercise.create_exercise(
        exercise_name, content_title, description, parent_id, grade, validation_only)
    return {
        'content_id': exercise_name,
        'errors': err_response
    }


def validate_or_update_exercise_by_edu_sheet(
        exercise_name, content_title, description, grade, validation_only):
    err_response = content.exercise.update_exercise(
        exercise_name, content_title, description, grade, validation_only)
    return {
        'content_id': exercise_name,
        'errors': err_response
    }


def create_section(parent_id, section_id, title, standalone_title):
    created_section_entity = content.section.create_section(parent_id, section_id, title, standalone_title)
    return {
        'id': created_section_entity['id'],
        'title': created_section_entity['title'],
        'standalone_title': created_section_entity['standalone_title'],
    }


def read_section(version_id, section_id):
    return content.section.read_section(section_id=section_id, version_id=version_id)


def update_section(section_id, new_section_id, new_title, new_standalone_title):
    content.section.update_section(section_id, new_section_id, new_title, new_standalone_title)


def validate_or_create_video_by_edu_sheet(readable_id, content_title, description, parent_id, grade, validation_only=True, version_id="edit"):
    errors = content.video.create_video(readable_id, content_title, description, parent_id, grade, validation_only, version_id)

    return {
        'readable_id': readable_id,
        'errors': errors
    }


def validate_or_update_video_by_edu_sheet(readable_id, content_title, description, grade, validation_only=True, version_id="edit"):
    errors = content.video.update_video(readable_id, content_title, description, grade, validation_only, version_id)

    return {
        'readable_id': readable_id,
        'errors': errors
    }


def video_play_data_impl(topic_id, video_id):
    return {"video": content.video.get_video_play(topic_id, video_id)}


def get_video(version_id: str, video_id: str) -> dict:
    return content.video.get_video(version_id, video_id)


def video_get_all_impl():
    ret = []
    for video in content.video.get_all_videos():
        ret.append({
            'display_name': video.title,
            'readable_id': video.readable_id,
        })
    return ret


def flush_instance_cache(category):
    if instance_cache._debug:
        instance_cache.flush(category=category)
        return 'ok'
    return 'fail'


def get_edu_sheet_topic_content_changes(subject):
    try:
        topic_change_table, content_change_table, errors = get_content_external_change([subject])
    except Exception as e:
        return {'errors': f"載入 google sheet API 有誤，請點擊「確認載入」重新載入資料。錯誤資訊： {e!r}"}

    if errors:
        return {'errors': errors}
    else:
        return {'topic_change_table': topic_change_table, 'content_change_table': content_change_table}


def warm_up():
    auth.user_util.get_cached_developer_list()
    auth.user_util.get_cached_moderator_list()
    return '', 200, {}


def get_topic_tree_data(version_id, update_cache=False):
    version = content.topic.get_version(version_id)
    return content.topic.get_topic_tree_data(version, bust_cache=update_cache) # pylint: disable=E1123


def get_topic_tree_data_with_publisher(version_id):
    version = content.topic.get_version(version_id)
    return content.topic.get_topic_tree_data_with_publisher(version)


def export_topic_tree_data(topic_id, version_id):
    version = content.topic.get_version(version_id)
    return content.topic.export_topic_tree_data(topic_id, version)


def _format_topic(data, n=1):
    ret = f'- {_format_item(data)}\n'
    try:
        for c in data['child_topics']:
            ret += '  ' * n
            ret += _format_topic(c, n + 1)
        for c in data['child_contents']:
            ret += '  ' * n
            ret += _format_content(c)
    except KeyError:
        pass
    return ret


def _format_content(data):
    return f'- #{data["kind"]} {_format_item(data)}\n'


def _format_item(data):
    ret = f'@{data["id"]} [{data["title"]}]'
    if not data['is_live']:
        ret += ' #hidden'
    return ret
