# -*- coding: utf-8 -*-
"""
Static data for parsing rows and common usage classes
"""
from abc import ABCMeta, abstractmethod
from types import LambdaType

from flask import current_app

TOPIC_KEY = {
    'ContentDomain': {
        'id_key': 'content_domain_id',
        'title_key': 'content_domain_title',
        'id': '代號',
        'title': '大主題',
    },
    'MainTopic': {
        'id_key': 'main_topic_id',
        'title_key': 'main_topic_title',
        'id': '代號',
        'title': '主題',
    },
    'Chapter': {
        'id_key': 'chapter_id',
        'title_key': 'chapter_title',
        'id': '代號',
        'title': '次主題、能力指標',
    },
}

CONTENT_ROW_MAP = [
    ['content_id', '代號'],
    ['content_title', lambda content: content['影片、知識點、講義'] if content['影片、知識點、講義'] else content['標題']],
    ['content_status', lambda content: content.get('建置情形', '')],
    ['description', '描述'],
    ['grade', '年級'],
]

TOPIC_ROW_MAP = [
    ['topic_id', '代號'],
    ['topic_status', lambda content: content.get('建置情形', '')],
    ['description', '描述'],
]

class SheetMeta(object):
    @staticmethod
    def CHANGE_STATUS_LIST():
        return ["執行上架", "編輯欄位", "調整位置", "執行下架"]

    @staticmethod
    def SUBJECTS_NEED_GRADE_INFO():
        return [
            'elem_math_3_to_6',
            'junior_math',
            'elem_math_3_to_6_test',
            'junior_math_test',
        ]

    @staticmethod
    def ALL_ALLOWED_STATUS_LIST():
        return ["執行上架", "編輯欄位", "調整位置", "執行下架", "已上架"]
    
    @staticmethod
    def CONTENT_CHANGE_COLUMN():
        return [
            'content_id',
            'content_title',
            'content_status',
            'content_kind',
            'parent_id',
            'description',
            'content_index',
            'grade',
        ]

    @staticmethod
    def TOPIC_CHANGE_COLUMN():
        return [
            'topic_id',
            'topic_title',
            'topic_status',
            'topic_kind',
            'parent_id',
            'description'
        ]

    @staticmethod
    def SHEET_IDS():
        # the link of the spreadsheet and the dictionary of the spreadsheet gids
        # the format is [spreadsheet, sheetid]
        if current_app.config.get('IN_LOCAL'):
            return {
                'elem_math_3_to_6': ["1jmc49Yossy9BZZzec8FIchLfTqSRGxniYXXO3lC1HbE", "1664493714"],
                'junior_math': ["1jmc49Yossy9BZZzec8FIchLfTqSRGxniYXXO3lC1HbE", "679570349"],
                'nhk_math': ["19h5EgtT51AyMdEYrZsJQWivIFWGq37TH1o1ijn9bY08", "0"],
                'nani_math': ["1WuF2F3i_Y8b6Uam-H7p1GDPioFddFSGlUu9bM6AWrvc", "0"],
                'junyi_competency': ["1eANWTtMBSpx9Cy2Vb5RQcTW2Lqg0cA7xjDOonbvU1b8", "0"],
                # 'elem_math_3_to_6_test' and 'junior_math_test' are for unit tests
                'elem_math_3_to_6_test': ['1jmc49Yossy9BZZzec8FIchLfTqSRGxniYXXO3lC1HbE', '1728654006'],
                'junior_math_test': ["1jmc49Yossy9BZZzec8FIchLfTqSRGxniYXXO3lC1HbE", "943587857"],
            }
        elif current_app.config.get('IN_TEST'):
            return {
                'elem_math_3_to_6': ["1jmc49Yossy9BZZzec8FIchLfTqSRGxniYXXO3lC1HbE", "1216702080"],
                'junior_math': ["1jmc49Yossy9BZZzec8FIchLfTqSRGxniYXXO3lC1HbE", "677509680"],
                'nhk_math': ["19h5EgtT51AyMdEYrZsJQWivIFWGq37TH1o1ijn9bY08", "0"],
                'nani_math': ["1WuF2F3i_Y8b6Uam-H7p1GDPioFddFSGlUu9bM6AWrvc", "0"],
                'junyi_competency': ["1eANWTtMBSpx9Cy2Vb5RQcTW2Lqg0cA7xjDOonbvU1b8", "0"],
            }
        return {
            'elem_math_3_to_6': ["1CcRo6t4VEXphR8JUl3wuAmInEgKAqiuPdRO6W5JpRzg", "2018162314"],
            'junior_math': ["1CcRo6t4VEXphR8JUl3wuAmInEgKAqiuPdRO6W5JpRzg", "66649370"],
            'nhk_math': ["19h5EgtT51AyMdEYrZsJQWivIFWGq37TH1o1ijn9bY08", "0"],
            'junior_physics_chemistry': ["1yrM59ckVxCpmq1sFQcHp9ib7CG-c1d9ZKK9EtuaMGzE", "96976649"],
            'junior_biology': ["118odn_nJ1bjU9zDBwt_72HEz6Q2a9cp9wji-Rp2uKbs", "576295800"],
            'junior_earth_science': ["1DytYjIY9ZMNEWKRB-Gv6nuQas1NX8Ny2-4OnzfurzyI", "277538246"],
            'senior_chemistry': ["18-GAzyrb9Dl45npdcAM5cPTgx29wYxNY8mP-_n8H6Xo", "0"],
            'nani_math': ["1WuF2F3i_Y8b6Uam-H7p1GDPioFddFSGlUu9bM6AWrvc", "0"],
            'junyi_competency': ["1eANWTtMBSpx9Cy2Vb5RQcTW2Lqg0cA7xjDOonbvU1b8", "0"],
        }

    @staticmethod
    def CLEAR_KEY_LIST():
        return [
            "main_topic_id",
            "main_topic_title",
            "chapter_id",
            "chapter_title",
            "section_title",
            "section_id",
            "section_id_by_grade",
            "grade"
        ]

    @staticmethod
    def CONTENT_TABLE_COL():
        return [
            "content_id", "content_title", "content_kind", "content_status", "content_index",
            "content_domain_id", "content_domain_title",
            "main_topic_id", "main_topic_title",
            "chapter_id", "chapter_title",
            "section_id", "section_id_by_grade", "section_title",
            "grade", "parent_id", "description"
        ]

    @staticmethod
    def TOPIC_TABLE_COL():
        return [
            "topic_id", "topic_title", "topic_kind", "topic_status",
            "content_domain_id", "content_domain_title",
            "main_topic_id", "main_topic_title",
            "chapter_id", "chapter_title",
            "section_id", "section_id_by_grade", "section_title",
            "grade", "parent_id", "description"
        ]

    @staticmethod
    def MATH_GRADE_CHINESE_WORDING(): 
        return {
            3: "數學 小三",
            4: "數學 小四",
            5: "數學 小五",
            6: "數學 小六",
            7: "數學 國一",
            8: "數學 國二",
            9: "數學 國三",
        }

class EduError(object):
    errors = []

    def add_error(self, error):
        self.errors.append(error)

class SheetTable(object):
    topic_row = {}

    content_row = {}

    topic_table = []

    content_table = []

    def __init__(self):
        self.topic_row = dict.fromkeys(SheetMeta.TOPIC_TABLE_COL(), "")
        self.content_row = dict.fromkeys(SheetMeta.CONTENT_TABLE_COL(), "")
        self.topic_table = []
        self.content_table = []

    def add_topic(self, row):
        self.topic_table.append(row)

    def add_content(self, row):
        self.content_table.append(row)


class SetRow(metaclass=ABCMeta):
    parent = ''

    topic_type = ''

    def __init__(self, sheet_table: SheetTable, edu_error: EduError):
        self.sheet_table = sheet_table
        self.edu_error = edu_error
        super(SetRow, self).__init__()

    def clear(self, clear_id = 0, clear_end = None):
        key_list = []
        if clear_end is None:
            key_list = SheetMeta.CLEAR_KEY_LIST()[clear_id:clear_end]
        else:
            key_list = SheetMeta.CLEAR_KEY_LIST()[clear_id:]

        for key in key_list:
            self.sheet_table.content_row[key] = ''
            self.sheet_table.topic_row[key] = ''

    def set_row(self, topic_dict):
        # Update current topic/content row with nearest ancestors' info by default
        # Function override is allowed
        id_key = TOPIC_KEY[self.topic_type]['id_key']
        title_key = TOPIC_KEY[self.topic_type]['title_key']
        id = TOPIC_KEY[self.topic_type]['id']
        title = TOPIC_KEY[self.topic_type]['title']

        self.sheet_table.topic_row[id_key] \
            = self.sheet_table.content_row[id_key] \
            = topic_dict[id]

        self.sheet_table.topic_row[title_key] \
            = self.sheet_table.content_row[title_key] \
            = self.sheet_table.topic_row['topic_title'] \
            = topic_dict[title]

    def copy_content_row(self, content, kind):
        self.sheet_table.content_row['content_kind'] = kind
        for key, value in CONTENT_ROW_MAP:
            if isinstance(value, LambdaType):
                self.sheet_table.content_row[key] = value(content)
            else:
                self.sheet_table.content_row[key] = content[value]
        self.sheet_table.content_row['parent_id'] = self.sheet_table.content_row['chapter_id']
        return self.sheet_table.content_row.copy()

    def copy_topic_row(self, content, kind):
        self.sheet_table.topic_row['topic_kind'] = kind

        for key, value in TOPIC_ROW_MAP:
            if isinstance(value, LambdaType):
                self.sheet_table.topic_row[key] = value(content)
            else:
                self.sheet_table.topic_row[key] = content[value]
        return self.sheet_table.topic_row.copy()

    def set_parent_id(self, parent_key):
        self.sheet_table.topic_row['parent_id'] \
            = self.sheet_table.topic_row[TOPIC_KEY[parent_key]['id_key']]

    @abstractmethod
    def set_topic(self, content, subject = ''):
        raise NotImplementedError

    def set_content(self, content, kind):
        self.sheet_table.add_content(
            self.copy_content_row(content, kind)
        )
        self.sheet_table.content_row['content_index'] += 1
