# -*- coding: utf-8 -*-
import unittest
import json
from unittest import mock

from api_implement import get_edu_sheet_topic_content_changes
from sheet.sheet import (
    get_content_external_change,
    modify_emath_row_ancestors_by_grade,
    modify_jmath_row_ancestors_by_grade,
)

def get_sheet_change_data():
    with open('sheet/internal/sheet_data.json', 'r') as f:
        return json.loads(f.read())

class TestSheetChange(unittest.TestCase):
    def setUp(self):
        with open('sheet/internal/sheet_emath_result.json', 'r') as f:
            self.emath_result = json.loads(f.read())
        with open('sheet/internal/sheet_jmath_result.json', 'r') as f:
            self.jmath_result = json.loads(f.read())
        sheet_change_data = get_sheet_change_data()
        self.sheet_change_data = sheet_change_data


    @mock.patch('sheet.sheet.get_info_content_external')
    def test_grade_error(self, mocked):
        mocked.return_value = [
            self.sheet_change_data['content_change_table'],
            self.sheet_change_data['topic_change_table'],
            ['次主題 【三年級】三角形[mesfl4a] 沒有填年級'],
        ]

        topic_change_table, \
        content_change_table, \
        problems_edu_team_table = get_content_external_change(['elem_math_3_to_6_test'])

        self.assertEqual(problems_edu_team_table, ['次主題 【三年級】三角形[mesfl4a] 沒有填年級'])

    @mock.patch('sheet.sheet.get_info_content_external')
    def test_get_content_external_change_exception(self, get_info_content_external_patch):
        e = Exception('test exception')
        msg = f'載入 google sheet API 有誤，請點擊「確認載入」重新載入資料。錯誤資訊： {e!r}'
        get_info_content_external_patch.side_effect = e
        result = get_edu_sheet_topic_content_changes('elem_math_3_to_3_test')
        self.assertEqual(msg, result['errors'])

    def test_wrong_learnstage(self):
        result = get_edu_sheet_topic_content_changes('elem_math_3_to_3_test')

        self.assertListEqual(result['topic_change_table'], [])
        self.assertListEqual(result['content_change_table'], [])
        self.assertNotIn('errors', result)

    def test_modify_math_by_grade(self):
        row = modify_emath_row_ancestors_by_grade({
            'grade': 7,
            'section_id_by_grade': 'new-topic-268',
            'chapter_id': 'new-topic-528',
            'chapter_title': '國中-數與量'.encode(),
            'main_topic_id': '',
            'main_topic_title': '',
            'content_domain_id': '',
            'content_domain_title': '',
        })

        self.assertEqual(row, {
            'grade': 7,
            'section_id_by_grade': 'new-topic-268',
            'chapter_id': 'g07-new-topic-528',
            'chapter_title': '國中-數與量',
            'section_id': 'new-topic-268',
            'main_topic_id': 'math-grade-7-a',
            'main_topic_title': '數學 國一',
            'content_domain_id': 'course-compare',
            'content_domain_title': '數學（年級式）',
        })

        row = modify_jmath_row_ancestors_by_grade({
            'grade': 4,
            'section_id_by_grade': 'new-topic-268',
            'chapter_id': 'new-topic-528',
            'main_topic_id': 'math-grade-4-a',
            'content_domain_id': '',
            'content_domain_title': '',
        })

        self.assertEqual(row, {
            'grade': 4,
            'section_id_by_grade': 'new-topic-268',
            'chapter_id': 'g04-new-topic-528',
            'main_topic_id': 'g-math-grade-4-a',
            'content_domain_id': 'math-grade-4-a',
            'content_domain_title': '數學 小四',
            'section_id': 'new-topic-268',
        })
