# -*- coding: utf-8 -*-
"""
API implements in order to load and parse data from google sheet
"""
import csv
import urllib
from copy import deepcopy

from .internal.sheet_change_implements import (
    SheetMeta,
    SetContentDomain,
    SetMainTopic,
    SetChapter,
    SetSection,
    SetContent,
)

from .internal.sheet_change import (
    EduError,
    SheetTable,
)

"""
dict: dict key for google sheet column, value for mapping in code
"""
CONTENT_TYPE_KIND = {
    '知識點': 'Exercise',
    '大知識點': 'Exercise',
    '影片': 'Video',
    '評量': 'Exam',
    '連結': 'Url',
}

def create_topic_strategies(sheet_table=SheetTable(), edu_error=EduError()):
    """Create strategies for topic and injects common instances

    Returns:
        dict: dict key for google sheet column, value for corresponding strategies
    """
    return {
        '大主題': SetContentDomain(sheet_table, edu_error),
        '主題': SetMainTopic(sheet_table, edu_error),
        '次主題': SetChapter(sheet_table, edu_error),
        '分隔線': SetSection(sheet_table, edu_error),
        'content': SetContent(sheet_table, edu_error),
    }

def dict_to_table(content_or_topic_dicts, subject, allowed_statuses):
    """
    Parse raw data from csv to corresponding row
    """
    sheet_table = SheetTable()
    edu_error = EduError()
    strategies = create_topic_strategies(sheet_table, edu_error)

    for content_or_topic_dict in content_or_topic_dicts:
        if content_or_topic_dict["建置情形"] not in allowed_statuses:
            continue

        content_type = content_or_topic_dict["類型"]
        if content_type in strategies:
            strategies[content_type].set_topic(content_or_topic_dict, subject)
        elif content_type in CONTENT_TYPE_KIND:
            kind = CONTENT_TYPE_KIND[content_type]
            strategies['content'].set_content(content_or_topic_dict, kind)
    return sheet_table.content_table, sheet_table.topic_table, edu_error.errors

def get_info_content_external(allowed_statuses, allowed_subjects=None):
    """
    Get data from google sheet by subjects
    """

    content_table = []
    topic_table = []
    errors = []

    subject_list = SheetMeta.SHEET_IDS().keys()
    if allowed_subjects:
        subject_list = list(subject for subject in subject_list if subject in allowed_subjects)

    for subject in subject_list:
        url = "https://docs.google.com/spreadsheets/export?id={}&format=csv&gid={}" \
            .format(SheetMeta.SHEET_IDS()[subject][0], SheetMeta.SHEET_IDS()[subject][1])
        request = urllib.request.Request(url)
        csvfile = urllib.request.urlopen(request).read().decode('utf-8').split('\r\n')

        subject_contents, subject_topics, subject_errors = dict_to_table(
            csv.DictReader(csvfile, delimiter=','),
            subject,
            allowed_statuses,
        )

        if subject in SheetMeta.SUBJECTS_NEED_GRADE_INFO():
            subject_contents += build_math_contents_by_grade(subject_contents, subject)
            subject_topics += build_math_topics_by_grade(subject_topics, subject)

        content_table += subject_contents
        topic_table += subject_topics
        errors += subject_errors

    return content_table, topic_table, errors

def get_content_external_change(allowed_subjects):
    """
    Get data and filter content
    """

    content_table, topic_table, errors = get_info_content_external(
        SheetMeta.ALL_ALLOWED_STATUS_LIST(),
        allowed_subjects,
    )

    content_change_table = []
    topic_change_table = []

    for content in content_table:
        if content["content_status"] in SheetMeta.CHANGE_STATUS_LIST():
            content_change_row = dict(
                (key, value)
                for key, value in content.items()
                if key in SheetMeta.CONTENT_CHANGE_COLUMN()
            )
            content_change_table.append(content_change_row)

    for topic in topic_table:
        if topic["topic_status"] in SheetMeta.CHANGE_STATUS_LIST():
            topic_change_row = dict(
                (key, value)
                for key, value in topic.items()
                if key in SheetMeta.TOPIC_CHANGE_COLUMN()
            )
            topic_change_table.append(topic_change_row)

    return topic_change_table, content_change_table, errors


def modify_emath_row_ancestors_by_grade(row):
    """
    把國小數學的 topic, content 的 ancestor topic 轉成年級式
    """
    row_grade = row["grade"]
    if not row_grade:
        return row

    row["section_id"] = row["section_id_by_grade"]
    row["chapter_id"] = "g%02d-"%(row_grade) + row["chapter_id"]
    row["chapter_title"] = row["chapter_title"].decode('utf-8').split(u"年級】")[-1]
    row["main_topic_id"] = "math-grade-%d-a"%(row_grade)
    row["main_topic_title"] = SheetMeta.MATH_GRADE_CHINESE_WORDING()[row_grade]
    row["content_domain_id"] = "course-compare"
    row["content_domain_title"] = "數學（年級式）"

    return row

def modify_jmath_row_ancestors_by_grade(row):
    """
    把國中數學的 topic, content 的 ancestor topic 轉成年級式
    """
    row_grade = row["grade"]
    if not row_grade:
        return row

    row["section_id"] = row["section_id_by_grade"]
    row["chapter_id"] = "g%02d-"%(row_grade) + row["chapter_id"]
    row["main_topic_id"] = "g-" + row["main_topic_id"]
    row["content_domain_id"] = "math-grade-%d-a"%(row_grade)
    row["content_domain_title"] = SheetMeta.MATH_GRADE_CHINESE_WORDING()[row_grade]

    return row

def build_math_contents_by_grade(math_contents_by_topic, subject):
    """
    建出數學年級式 content_table。因為教育組目前只有主題式的 google sheet

    TODO(idid): 沿用 chapter_id 作為 section_id 的 content
        section_id_by_grade 會是 ""，導致 section_id 變成 ""
    """
    math_contents_by_grade = deepcopy(math_contents_by_topic)
    if subject == "elem_math_3_to_6" or subject == "elem_math_3_to_6_test":
        modify_row_ancestors_by_grade = modify_emath_row_ancestors_by_grade
    else: # handle subject == "junior_math"
        modify_row_ancestors_by_grade = modify_jmath_row_ancestors_by_grade

    for content in math_contents_by_grade:
        content = modify_row_ancestors_by_grade(content)
        content["parent_id"] = content["chapter_id"]

        if content["content_kind"] == 'Section':
            content["content_id"] = content["section_id"]

    return math_contents_by_grade

def build_math_topics_by_grade(math_topics_by_topic, subject):
    """
    建出數學年級式 topic_table。因為教育組目前只有主題式的 google sheet

    modify_row_ancestors_by_grade 只會改寫 ancestors 的資訊
    topic_id, topic_title, parent_id 需要另外改寫
    """
    math_topics_by_grade = deepcopy(math_topics_by_topic)
    if subject == "elem_math_3_to_6" or subject == "elem_math_3_to_6_test":
        modify_row_ancestors_by_grade = modify_emath_row_ancestors_by_grade
    else: # handle subject == "junior_math"
        modify_row_ancestors_by_grade = modify_jmath_row_ancestors_by_grade
    # 刪掉從主題式複製過來的 ContentDomain, MainTopic
    math_topics_by_grade = list(
        c for c in math_topics_by_grade if not (c['topic_kind'] in ['ContentDomain', 'MainTopic'])
    )

    for topic in math_topics_by_grade:
        topic = modify_row_ancestors_by_grade(topic)

        if topic["topic_kind"] == 'Chapter':
            topic["topic_id"] = topic["chapter_id"]
            topic["topic_title"] = topic["chapter_title"]
            topic["parent_id"] = topic["main_topic_id"]

    return math_topics_by_grade
