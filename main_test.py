# -*- coding: utf-8 -*-
import json
import os
import pickle
import unittest
from unittest.mock import patch

from google.cloud import datastore

import content.internal.internal
import content.internal.topic
import content.internal.video
import content.section
import main
from testutil.authutil import create_jaid


class TestXSFR(unittest.TestCase):
    def setUp(self):
        main.app.testing = True
        os.environ['FLASK_DEBUG'] = 'yes'
        self.client = main.app.test_client()

    def test_ok(self):
        fkey_value = 'xsrf_random_value'
        self.client.set_cookie('localhost', main.XSRF_COOKIE_KEY, fkey_value)
        r = self.client.get('/api/content/topictreedata?casing=camel', headers={main.XSRF_HEADER_KEY: fkey_value})
        self.assertEqual(r.status_code, 200)

    def test_no_header(self):
        fkey_value = 'xsrf_random_value'
        self.client.set_cookie('localhost', main.XSRF_COOKIE_KEY, fkey_value)
        r = self.client.get('/api/content/topictreedata?casing=camel')
        self.assertEqual(r.status_code, 400)

    def test_no_cookie(self):
        fkey_value = 'xsrf_random_value'
        r = self.client.get('/api/content/topictreedata?casing=camel', headers={main.XSRF_HEADER_KEY: fkey_value})
        self.assertEqual(r.status_code, 400)

    def test_mismatch(self):
        fkey_value = 'xsrf_random_value'
        self.client.set_cookie('localhost', main.XSRF_COOKIE_KEY, fkey_value + 'lala')
        r = self.client.get('/api/content/topictreedata?casing=camel', headers={main.XSRF_HEADER_KEY: fkey_value})
        self.assertEqual(r.status_code, 403)

    def test_exclude_cases(self):
        r = self.client.get('/api/content/hello')
        self.assertEqual(r.status_code, 200)

        r = self.client.get('/api/content/spec')
        self.assertEqual(r.status_code, 400)

        original_app_config = main.app.config['ENV']
        main.app.config['ENV'] = 'development'
        r = self.client.get('/api/content/spec')
        self.assertEqual(r.status_code, 200)

        main.app.config['ENV'] = original_app_config


class TestMainBase(unittest.TestCase):
    def setUp(self):
        main.app.testing = True
        os.environ['FLASK_DEBUG'] = 'yes'
        self.client = main.app.test_client()

    def fetch(self, url, method='get', data=None):
        fkey_value = 'xsrf_random_value'
        self.client.set_cookie('localhost', main.XSRF_COOKIE_KEY, fkey_value)
        headers = {main.XSRF_HEADER_KEY: fkey_value}
        if method.lower() == 'put':
            if data is not None:
                return self.client.put(
                    url,
                    headers=headers,
                    data=json.dumps(data),
                    content_type='application/json',
                )
            else:
                return self.client.put(url, headers=headers)
        if method.lower() == 'post':
            if data is not None:
                return self.client.post(
                    url,
                    headers=headers,
                    data=json.dumps(data),
                    content_type='application/json',
                )
            else:
                return self.client.post(url, headers=headers)
        return self.client.get(url, headers=headers)


class TestAppConfig(TestMainBase):
    def test_server_only_in_one_status(self):
        from flask import current_app

        status = [
            current_app.config.get('IN_LOCAL'),
            current_app.config.get('IN_LIVE'),
            current_app.config.get('IN_TEST'),
        ]
        self.assertEqual(
            len(list(filter(bool, status))),
            1
        )


class TestAccessControl(TestMainBase):
    def test_moderator_required_forbidden(self):
        r = self.fetch('/api/content/topictree/default/edu_sheet/topic/create', method='post')
        self.assertEqual(r.status_code, 401)

    def test_moderator_required_forbidden_2(self):
        self.client.set_cookie('localhost', 'JAID', create_jaid())
        r = self.fetch('/api/content/topictree/default/edu_sheet/topic/create', method='post')
        self.assertEqual(r.status_code, 403)

    """
    pass @moderator_required 進到 api 主程式就算 pass, 故意把版本放 default 是為了不要進到主程式更深的邏輯
    """

    def test_moderator_required_pass(self):
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True))
        r = self.fetch('/api/content/topictree/default/edu_sheet/topic/create', method='post')
        self.assertEqual(r.status_code, 400)

    def test_moderator_required_pass_2(self):
        self.client.set_cookie('localhost', 'JAID', create_jaid(developer=True))
        r = self.fetch('/api/content/topictree/default/edu_sheet/topic/create', method='post')
        self.assertEqual(r.status_code, 400)


class TestAPIDoc(TestMainBase):
    def setUp(self):
        super().setUp()
        self.original_app_config = main.app.config['ENV']
        main.app.config['ENV'] = 'development'

    def tearDown(self):
        super().tearDown()
        main.app.config['ENV'] = self.original_app_config

    def test_api_spec(self):
        r = self.fetch('/api/content/spec')
        data = json.loads(r.data.decode('utf-8'))
        self.assertGreater(len(data['paths']), 3)

        for key in data['paths']:
            api_path = data['paths'][key]
            for method in api_path:
                # Check the required member
                self.assertFalse(api_path[method]['summary'] == '')
                self.assertTrue(len(api_path[method]['responses']) > 0)

    def test_api_doc(self):
        r = self.fetch('/api/content/doc')
        self.assertEqual(r.status_code, 200)


class TestTopic(TestMainBase):
    def test_no_parameters(self):
        r = self.fetch('/api/content/topic')
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'),
                         'Require [version_id] and [topic_type] GET parameters')

    def test_invalid_version(self):
        r = self.fetch('/api/content/topic?version_id=a&topic_type=exercise_topic')
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'),
                         'Require [version_id] and [topic_type] GET parameters')

    def test_invalid_type(self):
        r = self.fetch('/api/content/topic?version_id=350&topic_type=apple')
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), 'Invalid topic_type')

    def test_ok_v350(self):
        """
        測試資料庫不太可能改到舊版的 topic version，所以這邊測的比較嚴
        """
        r = self.fetch('/api/content/topic?version_id=350&topic_type=exercise_topic')
        self.assertEqual(r.status_code, 200)

        resp_headers = r.headers
        self.assertEqual(resp_headers['Content-Type'], 'application/json')
        self.assertEqual(int(resp_headers['Content-Length']), 85803)

        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(len(resp_json), 56)
        first_topic = resp_json[0]
        self.assertEqual(first_topic['id'], 'junyi-eco-systems')
        self.assertEqual(first_topic['entity_key_name'],
                         'niZXiXe5Qew5EYNbM3J5Gta9xSABeIV_rz2C9n96')
        self.assertEqual(first_topic['version']['number'], 350)

        self.assertEqual(len(resp_json[6]['children']), 12)
        self.assertEqual(resp_json[6]['children'][0]['name'], 'trigonometry_0.5')


class TestTopicPage(TestMainBase):
    def test_normal_user_ok(self):
        self.client.set_cookie('localhost', 'JAID', create_jaid())
        r = self.fetch('/api/content/topicpage?topic_id=junyi-middle-school-biology')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.headers['Content-Type'], 'application/json')
        data = json.loads(r.data.decode('utf-8'))
        self.assertIsNotNone(data)
        self.assertEqual(data['title'], '國中生物')
        self.assertEqual(len(data['child']), 12)
        self.assertEqual(list(data.keys()), ['banner_src', 'breadcrumb', 'child', 'description', 'extended_slug',
                                             'icon_src', 'intro', 'is_content_topic', 'links',
                                             'logo_src', 'tags', 'title', 'topic_id'])

    def test_grand_child_ok(self):
        self.client.set_cookie('localhost', 'JAID', create_jaid())

        r = self.fetch('/api/content/topicpage?topic_id=junyi-math')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.headers['Content-Type'], 'application/json')
        data = json.loads(r.data.decode('utf-8'))
        self.assertIsNotNone(data)
        self.assertEqual(data['title'], '數學（主題式）')
        child = data['child']
        self.assertEqual(len(child), 8)
        # each child has at most 6 grandchildren
        self.assertEqual([len(d['child']) for d in child], [6, 6, 0, 6, 6, 1, 6, 0])

        r = self.fetch('/api/content/topicpage?topic_id=junyi-elementary-math')
        data = json.loads(r.data.decode('utf-8'))
        self.assertEqual(data['title'], '算數')
        # the first child of `junyi-math`: `junyi-elementary-math` has 15 children
        self.assertEqual(len(data['child']), 15)

    def test_grand_child_ok_v1(self):
        self.client.set_cookie('localhost', 'JAID', create_jaid())

        r = self.fetch('/api/content/v1/topicpage?topic_id=junyi-math')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.headers['Content-Type'], 'application/json')
        data = json.loads(r.data.decode('utf-8'))
        self.assertIsNotNone(data)
        self.assertEqual(data['title'], '數學（主題式）')
        child = data['child']
        self.assertEqual(len(child), 8)
        # each child has at most 6 grandchildren
        self.assertEqual([len(d['child']) for d in child], [6, 6, 0, 6, 6, 1, 6, 0])
        self.assertEqual(set(child[0]['child'][0].keys()), {'topic_id', 'title'})

        r = self.fetch('/api/content/topicpage?topic_id=junyi-elementary-math')
        data = json.loads(r.data.decode('utf-8'))
        self.assertEqual(data['title'], '算數')
        # the first child of `junyi-math`: `junyi-elementary-math` has 15 children
        self.assertEqual(len(data['child']), 15)

    @patch('content.internal.internal.get_edit_version')
    def test_no_edit_ver_ok(self, get_edit_version_patch):
        get_edit_version_patch.side_effect = content.internal.internal.InvalidContentTree
        self.client.set_cookie('localhost', 'JAID', create_jaid())
        r = self.fetch('/api/content/topicpage?topic_id=junyi-middle-school-biology')
        data = json.loads(r.data.decode('utf-8'))
        self.assertEqual(data['title'], '國中生物')
        self.assertEqual(len(data['child']), 12)

    def test_normal_user_access_hidden_topic(self):
        self.client.set_cookie('localhost', 'JAID', create_jaid())
        r = self.fetch('/api/content/topicpage?topic_id=khan-videos')
        self.assertEqual(r.status_code, 404)
        self.assertEqual(r.data.decode('utf-8'), 'No Topic for topic_id [khan-videos] in default version')

    def test_developer_access_hidden_topic(self):
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True, developer=True))
        r = self.fetch('/api/content/topicpage?topic_id=khan-videos')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.headers['Content-Type'], 'application/json')
        data = json.loads(r.data.decode('utf-8'))
        self.assertIsNotNone(data)
        self.assertEqual(data['title'], 'khan videos [hidden]')
        self.assertEqual(len(data['child']), 29)

    def test_topic_with_section_ok(self):
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True, developer=True))
        r = self.fetch('/api/content/topicpage?topic_id=junyi-addition-and-subtraction')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.headers['Content-Type'], 'application/json')
        data = json.loads(r.data.decode('utf-8'))
        self.assertIsNotNone(data)
        self.assertEqual(data['title'], '加法與減法')
        self.assertTrue(data['is_content_topic'])
        self.assertEqual(data['child_video_count'], 4)
        self.assertEqual(data['child_exercise_count'], 1)
        self.assertEqual(len(data['child']), 7)

    def test_no_topic_id(self):
        r = self.fetch('/api/content/topicpage')
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), 'Require [topic_id] GET param')

    def test_wrong_topic_id(self):
        r = self.fetch('/api/content/topicpage?topic_id=ba')
        self.assertEqual(r.status_code, 404)
        self.assertEqual(r.data.decode('utf-8'), 'No Topic for topic_id [ba] in default version')

    def test_get_edit_ver_ok(self):
        update_id = 'test_update_topic'
        update_title = 'Test Update Topic Title'
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True))
        r = self.fetch('/api/content/topicpage?topic_id=' + update_id + '&v=edit')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.headers['Content-Type'], 'application/json')
        data = json.loads(r.data.decode('utf-8'))
        self.assertIsNotNone(data)
        self.assertEqual(data['title'], update_title)

    def test_get_edit_ver_fail_user(self):
        update_id = 'test_update_topic'
        self.client.set_cookie('localhost', 'JAID', create_jaid())
        r = self.fetch('/api/content/topicpage?topic_id=' + update_id + '&v=edit')
        self.assertEqual(r.status_code, 401)
        self.assertEqual(r.data.decode('utf-8'), 'Only moderators are allowed to get edit tree.')


class TestTopicTreeData(TestMainBase):
    def test_ok_current_default(self):
        """
        隨著測試資料庫有修改，回傳值會改變，所以這邊測的比較鬆
        """
        r = self.fetch('/api/content/topictreedata?casing=camel')
        self.assertEqual(r.status_code, 200)

        resp_headers = r.headers
        self.assertEqual(resp_headers['Content-Type'], 'application/json')
        self.assertTrue(int(resp_headers['Content-Length']) > 63500)

        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(len(resp_json.keys()), 6)
        self.assertEqual(resp_json['id'], 'root')
        self.assertEqual(resp_json['title'], 'The Root of All Knowledge')
        self.assertIsNotNone(resp_json['key'])
        self.assertListEqual(resp_json['tags'], [])
        self.assertFalse(resp_json['isContentTopic'])
        self.assertEqual(len(resp_json['childTopics']), 8)

    def test_ok_v350(self):
        """
        測試資料庫不太可能改到舊版的 topic version，所以這邊測的比較嚴
        """
        r = self.fetch('/api/content/topictreedata?casing=camel&version_id=350')
        self.assertEqual(r.status_code, 200)

        resp_headers = r.headers
        self.assertEqual(resp_headers['Content-Type'], 'application/json')
        self.assertEqual(int(resp_headers['Content-Length']), 64242)

        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(len(resp_json.keys()), 6)
        self.assertEqual(resp_json['id'], 'root')
        self.assertEqual(resp_json['title'], 'The Root of All Knowledge')
        self.assertEqual(resp_json['key'], 'ahBkZXZ-anVueWlhY2FkZW15cjMLEgVUb3BpYyIodmsye'
                                           'UxpX1Z6Zkx0bkFfbmNhMzZXY243SndJNjZDQTdVVE1tNnp3SQw')
        self.assertListEqual(resp_json['tags'], [])
        self.assertFalse(resp_json['isContentTopic'])
        self.assertEqual(len(resp_json['childTopics']), 7)

        # subtree 抽查
        self.assertEqual(resp_json['childTopics'][3]['id'], 'arts-and-humanities')
        self.assertTrue(resp_json['childTopics'][3]['childTopics'][0]['childTopics'][0]['isContentTopic'])

    def test_not_camel(self):
        r = self.fetch('/api/content/topictreedata')
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), 'Error: Not support other casing!')

    def test_invalid_version(self):
        r = self.fetch('/api/content/topictreedata?casing=camel&version_id=a')
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'),
                         'Invalid version_id. [a]')

    @patch('content.internal.topic.get_root_key')
    def test_invalid_root(self, get_root_key_patch):
        get_root_key_patch.return_value = content.internal.internal.get_client().key('Topic',
                                                                                     'wrong_root_key')
        r = self.fetch('/api/content/topictreedata?casing=camel&version_id=350')
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'),
                         'root of version [350] not exists')


class TestTopicWriteDB(TestMainBase):
    update_id = 'test_update_topic'
    update_title = 'Test Update Topic Title'
    update_desc = 'a fake topic to test update function'
    parent_id = 'math'
    create_id = 'new_topic_id'
    create_title = 'New Topic Title'
    create_desc = 'description of new topic'
    db_changed_update = False
    db_changed_create = False

    @staticmethod
    def _get_topic_entity_by_id_retry(topic_id, version, root_key, check_fn=None):
        if check_fn is None:
            check_fn = lambda e: e is not None
        retry_count = 0
        entity = None
        while not check_fn(entity) and retry_count < 5:
            entity = content.internal.topic._get_by_id(topic_id, version, root_key)
            retry_count += 1
        return entity

    def get_topic_entity_by_id(self, topic_id):
        edit_version = content.internal.internal.get_edit_version()
        root_key = content.internal.topic.get_root_key(edit_version)
        return self._get_topic_entity_by_id_retry(topic_id, edit_version, root_key)

    def tearDown(self):
        super().tearDown()
        # revert 修改到的 topic
        if self.db_changed_update:
            content.internal.topic.update_topic(
                False, self.update_id, id=self.update_id, title=self.update_title,
                standalone_title=self.update_title, description=self.update_desc)
        # revert 建出的 topic
        if self.db_changed_create:
            e = self.get_topic_entity_by_id(self.create_id)
            client = content.internal.internal.get_client()
            parents = client.get_multi(e['parent_keys'])
            for parent in parents:
                parent['child_keys'].remove(e.key)
                client.put(parent)
            client.delete(e.key)


class TestTopicCreate(TestTopicWriteDB):

    def setUp(self):
        super().setUp()
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True))

    def make_payload(self):
        return {
            'topic_title': self.create_title,
            'description': self.create_desc,
            'parent_id': self.parent_id,
            'validation_only': False
        }

    @patch('google.cloud.datastore.Client.put')
    def test_create_ok_with_topic_id(self, put_patch):
        data = self.make_payload()
        r = self.fetch(f'/api/content/v1/topicversions/edit/topics/{self.create_id}', method='put', data=data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertTrue(put_patch.called)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(resp_json['errors']), 0)
        self.assertEqual(resp_json['topic_id'], self.create_id)

    @patch('google.cloud.datastore.Client.put')
    def test_create_ok_without_topic_id(self, put_patch):
        data = self.make_payload()
        r = self.fetch(f'/api/content/v1/topicversions/edit/topics', method='post', data=data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertTrue(put_patch.called)
        self.assertEqual(r.status_code, 201)
        self.assertEqual(len(resp_json['errors']), 0)
        self.assertIn('new-topic-title', resp_json['topic_id'])

    @patch('google.cloud.datastore.Client.put')
    def test_create_fail_without_parent_id(self, put_patch):
        data = self.make_payload()
        del data['parent_id']
        r = self.fetch(f'/api/content/v1/topicversions/edit/topics/{self.create_id}', method='put', data=data)
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), 'Missing required parameter: parent_id')
        self.assertFalse(put_patch.called)

    @patch('google.cloud.datastore.Client.put')
    def test_create_fail_with_invalid_version(self, put_patch):
        data = self.make_payload()
        topic_version = 'invalid_topic_version'
        r = self.fetch(f'/api/content/v1/topicversions/{topic_version}/topics/{self.create_id}'
                       , method='put', data=data)
        self.assertEqual(r.status_code, 400)
        print(r.data.decode('utf-8'))
        self.assertEqual(r.data.decode('utf-8'), '目前禁止於 edit 以外的版本建立 Topic')
        self.assertFalse(put_patch.called)

    @patch('google.cloud.datastore.Client.put')
    def test_create_fail_with_invalid_parent_id(self, put_patch):
        data = self.make_payload()
        data['parent_id'] = data['parent_id'] + 'x'
        r = self.fetch(f'/api/content/v1/topicversions/edit/topics/{self.create_id}', method='put', data=data)
        self.assertFalse(put_patch.called)
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), '預計擺放的母資料夾 [%s] 不存在' % data['parent_id'])

    @patch('google.cloud.datastore.Client.put')
    def test_create_fail_with_existing_topic_id(self, put_patch):
        data = self.make_payload()
        r = self.fetch(f'/api/content/v1/topicversions/edit/topics/{self.update_id}', method='put', data=data)
        self.assertEqual(r.status_code, 400)
        self.assertEqual(
            r.data.decode('utf-8'),
            '已存在代號是 %s 的資料夾，該資料夾的標題是 [%s]' % (self.update_id, self.update_title)
        )
        self.assertFalse(put_patch.called)

    @patch('google.cloud.datastore.Client.put')
    def test_create_without_authorization(self, put_patch):
        self.client.set_cookie('localhost', 'JAID', '')
        data = self.make_payload()
        r = self.fetch(f'/api/content/v1/topicversions/edit/topics', method='post', data=data)
        self.assertEqual(r.status_code, 401)
        self.assertFalse(put_patch.called)


class TestTopicEduSheet(TestTopicWriteDB):

    @patch('google.cloud.datastore.Client.put')
    def test_default_version(self, put_patch):
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True))
        r = self.fetch('/api/content/topictree/default/edu_sheet/topic/update?validation_only=1&',
                       method='put')
        self.assertFalse(put_patch.called)
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), '目前禁止於 edit 以外的版本修改 Topic')
        r = self.fetch('/api/content/topictree/default/edu_sheet/topic/create?validation_only=1&',
                       method='post')
        self.assertFalse(put_patch.called)
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), '目前禁止於 edit 以外的版本建立 Topic')

    def test_update_ok_put_db(self):
        self.db_changed_update = True
        # 確認原本的 DB
        topic_entity = self.get_topic_entity_by_id(self.update_id)
        self.assertEqual(topic_entity['id'], self.update_id)
        self.assertEqual(topic_entity['title'], self.update_title)
        self.assertEqual(topic_entity['description'], self.update_desc)
        # 修改 topic
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True))
        data = {
            'topic_id': self.update_id,
            'topic_title': self.update_title + 'x',
            'description': self.update_desc + 'x',
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/topic/update?validation_only=0&',
                       method='put', data=data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(resp_json['errors']), 0)
        self.assertEqual(resp_json['topic_id'], self.update_id)
        # 確認有修改到 DB
        for i in range(5):
            topic_entity = self.get_topic_entity_by_id(self.update_id)
            if topic_entity['title'] == data['topic_title']:
                break
            print('...retry %s' % (i + 1))
        self.assertEqual(topic_entity['id'], data['topic_id'])
        self.assertEqual(topic_entity['title'], data['topic_title'])
        self.assertEqual(topic_entity['description'], data['description'])

    @patch('google.cloud.datastore.Client.put')
    def test_update_ok(self, put_patch):
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True))

        # 測試沒改變
        data = {
            'topic_id': self.update_id,
            'topic_title': self.update_title,
            'description': self.update_desc,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/topic/update?validation_only=1&',
                       method='put', data=data)
        self.assertFalse(put_patch.called)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(resp_json['errors']), 0)
        self.assertEqual(resp_json['topic_id'], self.update_id)
        r = self.fetch('/api/content/topictree/edit/edu_sheet/topic/update?validation_only=0&',
                       method='put', data=data)
        self.assertFalse(put_patch.called)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(resp_json['errors']), 0)
        self.assertEqual(resp_json['topic_id'], self.update_id)

        # 測試有改變
        data = {
            'topic_id': self.update_id,
            'topic_title': self.update_title + 'x',
            'description': self.update_desc + 'x',
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/topic/update?validation_only=1&',
                       method='put', data=data)
        self.assertFalse(put_patch.called)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(resp_json['errors']), 0)
        self.assertEqual(resp_json['topic_id'], self.update_id)
        r = self.fetch('/api/content/topictree/edit/edu_sheet/topic/update?validation_only=0&',
                       method='put', data=data)
        self.assertTrue(put_patch.called)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(resp_json['errors']), 0)
        self.assertEqual(resp_json['topic_id'], self.update_id)

    @patch('google.cloud.datastore.Client.put')
    def test_update_fail(self, put_patch):
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True))

        # 測試 id 錯誤
        data = {
            'topic_id': self.update_id + 'x',
            'topic_title': self.update_title,
            'description': self.update_desc,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/topic/update?validation_only=0&',
                       method='put', data=data)
        self.assertFalse(put_patch.called)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(resp_json['errors']), 1)
        self.assertEqual(resp_json['errors'][0], '這個代號的資料夾不存在 %s' % data['topic_id'])

    def test_create_ok_put_db(self):
        self.db_changed_create = True
        # 確認原本的 DB
        topic_entity = self.get_topic_entity_by_id(self.create_id)
        self.assertIsNone(topic_entity)
        # 建立 topic
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True))
        data = {
            'topic_id': self.create_id,
            'topic_title': self.create_title,
            'description': self.create_desc,
            'parent_id': self.parent_id,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/topic/create?validation_only=0&',
                       method='post', data=data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(resp_json['errors']), 0)
        self.assertEqual(resp_json['topic_id'], self.create_id)
        # 確認有修改到 DB
        topic_entity = self.get_topic_entity_by_id(self.create_id)
        self.assertEqual(topic_entity['id'], data['topic_id'])
        self.assertEqual(topic_entity['title'], data['topic_title'])
        self.assertEqual(topic_entity['description'], data['description'])

    @patch('google.cloud.datastore.Client.put')
    def test_create_ok(self, put_patch):
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True))

        # 測試建立
        data = {
            'topic_id': self.create_id,
            'topic_title': self.create_title,
            'description': self.create_desc,
            'parent_id': self.parent_id,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/topic/create?validation_only=1&',
                       method='post', data=data)
        self.assertFalse(put_patch.called)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(resp_json['errors']), 0)
        self.assertEqual(resp_json['topic_id'], self.create_id)
        r = self.fetch('/api/content/topictree/edit/edu_sheet/topic/create?validation_only=0&',
                       method='post', data=data)
        self.assertTrue(put_patch.called)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(resp_json['errors']), 0)
        self.assertEqual(resp_json['topic_id'], self.create_id)

    @patch('google.cloud.datastore.Client.put')
    def test_create_fail(self, put_patch):
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True))

        # 測試沒給 parent id
        data = {
            'topic_id': self.create_id,
            'topic_title': self.create_title,
            'description': self.create_desc,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/topic/create?validation_only=0&',
                       method='post', data=data)
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), 'Missing required parameter: parent_id')

        # 測試 parent id 錯誤
        data = {
            'topic_id': self.create_id,
            'topic_title': self.create_title,
            'description': self.create_desc,
            'parent_id': self.parent_id + 'x',
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/topic/create?validation_only=0&',
                       method='post', data=data)
        self.assertFalse(put_patch.called)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(resp_json['errors']), 1)
        self.assertEqual(resp_json['errors'][0], '預計擺放的母資料夾 [%s] 不存在' % data['parent_id'])

        # 測試 new id 錯誤
        data = {
            'topic_id': self.update_id,
            'topic_title': self.create_title,
            'description': self.create_desc,
            'parent_id': self.parent_id,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/topic/create?validation_only=0&',
                       method='post', data=data)
        self.assertFalse(put_patch.called)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(resp_json['errors']), 1)
        self.assertRegex(resp_json['errors'][0], '已存在代號是 %s 的資料夾' % data['topic_id'])


class TestExerciseEduSheet(TestMainBase):
    # The testing exercise to be created.
    test_ex_name = 'test_exercise'
    test_ex_title = 'Test Exercise'
    test_ex_desc = 'A dummy exercise for testing.'
    test_ex_parent_ids = ['math', 'root']
    test_ex_grade = '3'

    def _remove_test_exercise(self):
        exercise = content.internal.exercise.get_by_name(self.test_ex_name)
        if exercise is None:
            return

        client = content.internal.internal.get_client()
        version = content.internal.internal.get_edit_version()
        self.assertIsNotNone(version)
        root_key = content.internal.topic.get_root_key(version)

        with client.transaction():
            for parent_id in self.test_ex_parent_ids:
                parent_topic = content.internal.topic._get_by_id(parent_id, version, ancestor=root_key)
                self.assertIsNotNone(parent_topic)
                if exercise.key in parent_topic['child_keys']:
                    parent_topic['child_keys'].remove(exercise.key)
                    client.put(parent_topic)

            change = content.internal.exercise.get_content_change(exercise, version)
            if change is not None:
                client.delete(change.key)

            client.delete(exercise.key)

    def setUp(self):
        super().setUp()
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True))
        self._remove_test_exercise()

    def tearDown(self):
        self._remove_test_exercise()
        super().tearDown()

    def test_list_all_live_exercise_name(self):
        resp = self.fetch(
            '/api/content/exercises',
            method='get',
        )
        self.assertEqual(resp.status_code, 200)
        self.assertIsInstance(resp.json, list)
        for names in resp.json:
            self.assertIn(
                'name',
                names
            )
            self.assertIn(
                'display_name',
                names
            )

    @patch('content.internal.exercise.Exercise.get_all_use_cache')
    def test_list_all_exercise_name(self, mock_get_all_use_cache):
        mock_get_all_use_cache.return_value = []
        self.fetch(
            '/api/content/exercises?live_only=False',
            method='get',
        )
        mock_get_all_use_cache.assert_called_with(
            live_only=False, include_exam=False
        )

    @patch('google.cloud.datastore.Client.put')
    def test_default_version(self, put_patch):
        r = self.fetch('/api/content/topictree/default/edu_sheet/exercise/create?validation_only=0&',
                       method='post')
        self.assertFalse(put_patch.called)
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), "目前禁止於 edit 以外的版本建立 Exercise")

        r = self.fetch('/api/content/topictree/default/edu_sheet/exercise/update?validation_only=0&',
                       method='put')
        self.assertFalse(put_patch.called)
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), "目前禁止於 edit 以外的版本修改 Exercise")

    def test_create_ok(self):
        # Create test exercise.
        data = {
            'exercise_name': self.test_ex_name,
            'content_title': self.test_ex_title,
            'description': self.test_ex_desc,
            'parent_id': self.test_ex_parent_ids[0],
            'grade': self.test_ex_grade,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/exercise/create?validation_only=0&',
                       method='post', data=data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(resp_json['errors'], [])
        self.assertEqual(resp_json['content_id'], data['exercise_name'])

        # Validate DB.
        exercise = content.internal.exercise.get_by_name(data['exercise_name'])
        self.assertIsNotNone(exercise)
        self.assertEqual(exercise['name'], data['exercise_name'])
        self.assertEqual(exercise['pretty_display_name'], data['content_title'])
        self.assertEqual(exercise['description'], data['description'])

        version = content.internal.internal.get_edit_version()
        self.assertIsNotNone(version)
        parent_topic = content.internal.topic._get_by_id(data['parent_id'], version)
        self.assertIsNotNone(parent_topic)
        self.assertEqual(parent_topic['child_keys'].count(exercise.key), 1)

        change = content.internal.exercise.get_content_change(exercise, version)
        self.assertIsNone(change)

    @patch('google.cloud.datastore.Client.put')
    def test_create_fail(self, put_patch):
        # Invalid exercise name.
        data = {
            'exercise_name': '',
            'content_title': self.test_ex_title,
            'description': self.test_ex_desc,
            'parent_id': self.test_ex_parent_ids[0],
            'grade': self.test_ex_grade,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/exercise/create?validation_only=0&',
                       method='post', data=data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertFalse(put_patch.called)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(resp_json['content_id'], data['exercise_name'])
        self.assertEqual(resp_json['errors'], ["代號不能空白"])

        # Invalid content title.
        data = {
            'exercise_name': self.test_ex_name,
            'content_title': '',
            'description': self.test_ex_desc,
            'parent_id': self.test_ex_parent_ids[0],
            'grade': self.test_ex_grade,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/exercise/create?validation_only=0&',
                       method='post', data=data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertFalse(put_patch.called)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(resp_json['content_id'], data['exercise_name'])
        self.assertEqual(resp_json['errors'], ["標題不能空白"])

        # Invalid parent id.
        data = {
            'exercise_name': self.test_ex_name,
            'content_title': self.test_ex_title,
            'description': self.test_ex_desc,
            'parent_id': 'unknown_topic_id',
            'grade': self.test_ex_grade,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/exercise/create?validation_only=0&',
                       method='post', data=data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertFalse(put_patch.called)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(resp_json['content_id'], data['exercise_name'])
        self.assertEqual(resp_json['errors'], ["預計擺放的母資料夾 [%s] 不存在" % data['parent_id']])

        # Invalid grade. Grade is not interger.
        data = {
            'exercise_name': self.test_ex_name,
            'content_title': self.test_ex_title,
            'description': self.test_ex_desc,
            'parent_id': self.test_ex_parent_ids[0],
            'grade': 'invalid_grade',
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/exercise/create?validation_only=0&',
                       method='post', data=data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertFalse(put_patch.called)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(resp_json['content_id'], data['exercise_name'])
        self.assertEqual(resp_json['errors'], ["年級欄位為必填，且必須填寫 0 ~ 12 的整數"])

        # Invalid grade. Grade is out of range.
        data = {
            'exercise_name': self.test_ex_name,
            'content_title': self.test_ex_title,
            'description': self.test_ex_desc,
            'parent_id': self.test_ex_parent_ids[0],
            'grade': '13',
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/exercise/create?validation_only=0&',
                       method='post', data=data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertFalse(put_patch.called)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(resp_json['content_id'], data['exercise_name'])
        self.assertEqual(resp_json['errors'], ["年級必須填寫 0 ~ 12 的整數"])

    def test_create_with_existing_ok(self):
        # Create test exercise.
        data = {
            'exercise_name': self.test_ex_name,
            'content_title': self.test_ex_title,
            'description': self.test_ex_desc,
            'parent_id': self.test_ex_parent_ids[0],
            'grade': self.test_ex_grade,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/exercise/create?validation_only=0&',
                       method='post', data=data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(resp_json['errors'], [])
        self.assertEqual(resp_json['content_id'], data['exercise_name'])

        # Create the same exercise under another parent topic.
        new_data = {
            'exercise_name': self.test_ex_name,
            'content_title': self.test_ex_title,
            'description': self.test_ex_desc + ' (new)',
            'parent_id': self.test_ex_parent_ids[1],
            'grade': self.test_ex_grade,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/exercise/create?validation_only=0&',
                       method='post', data=new_data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(resp_json['errors'], [])
        self.assertEqual(resp_json['content_id'], new_data['exercise_name'])

        # Validate DB.
        exercise = content.internal.exercise.get_by_name(data['exercise_name'])
        self.assertIsNotNone(exercise)
        self.assertEqual(exercise['name'], data['exercise_name'])
        self.assertEqual(exercise['pretty_display_name'], data['content_title'])
        self.assertEqual(exercise['description'], data['description'])

        version = content.internal.internal.get_edit_version()
        self.assertIsNotNone(version)
        parent_topic = content.internal.topic._get_by_id(data['parent_id'], version)
        self.assertIsNotNone(parent_topic)
        self.assertEqual(parent_topic['child_keys'].count(exercise.key), 1)
        new_parent_topic = content.internal.topic._get_by_id(new_data['parent_id'], version)
        self.assertIsNotNone(new_parent_topic)
        self.assertEqual(new_parent_topic['child_keys'].count(exercise.key), 1)

        change = content.internal.exercise.get_content_change(exercise, version)
        changed_data = pickle.loads(change['content_changes'])
        self.assertIsNotNone(change)
        self.assertEqual(change['version'], version.key)
        self.assertEqual(change['content'], exercise.key)
        self.assertTrue('pretty_display_name' not in changed_data)
        self.assertEqual(changed_data['description'], new_data['description'])

    def test_create_with_existing_fail(self):
        # Create test exercise.
        data = {
            'exercise_name': self.test_ex_name,
            'content_title': self.test_ex_title,
            'description': self.test_ex_desc,
            'parent_id': self.test_ex_parent_ids[0],
            'grade': self.test_ex_grade,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/exercise/create?validation_only=0&',
                       method='post', data=data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(resp_json['content_id'], data['exercise_name'])
        self.assertEqual(resp_json['errors'], [])

        # Create in the same parent topic.
        new_data = {
            'exercise_name': self.test_ex_name,
            'content_title': self.test_ex_title,
            'description': self.test_ex_desc + ' (new)',
            'parent_id': self.test_ex_parent_ids[0],
            'grade': self.test_ex_grade,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/exercise/create?validation_only=0&',
                       method='post', data=new_data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(resp_json['content_id'], new_data['exercise_name'])
        self.assertEqual(resp_json['errors'], ["母資料夾中，已存在相同標題的知識點 [%s]" % data['content_title']])

        # Create with different title.
        new_data = {
            'exercise_name': self.test_ex_name,
            'content_title': self.test_ex_title + ' (new)',
            'description': self.test_ex_desc + ' (new)',
            'parent_id': self.test_ex_parent_ids[1],
            'grade': self.test_ex_grade,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/exercise/create?validation_only=0&',
                       method='post', data=new_data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(resp_json['content_id'], new_data['exercise_name'])
        self.assertEqual(resp_json['errors'], ["此知識點已存在，且已存在的知識點標題 [%s] 和預計上架的標題不一樣" % data['content_title']])

    def test_update_ok(self):
        # Create test exercise.
        data = {
            'exercise_name': self.test_ex_name,
            'content_title': self.test_ex_title,
            'description': self.test_ex_desc,
            'parent_id': self.test_ex_parent_ids[0],
            'grade': self.test_ex_grade,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/exercise/create?validation_only=0&',
                       method='post', data=data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(resp_json['errors'], [])
        self.assertEqual(resp_json['content_id'], data['exercise_name'])

        # Update test exercise.
        new_data = {
            'exercise_name': self.test_ex_name,
            'content_title': self.test_ex_title + ' (updated)',
            'description': self.test_ex_desc + '(updated)',
            'grade': self.test_ex_grade,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/exercise/update?validation_only=0&',
                       method='put', data=new_data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(resp_json['errors'], [])
        self.assertEqual(resp_json['content_id'], data['exercise_name'])

        # Validate DB.
        exercise = content.internal.exercise.get_by_name(data['exercise_name'])
        self.assertIsNotNone(exercise)
        self.assertEqual(exercise['name'], data['exercise_name'])
        self.assertEqual(exercise['pretty_display_name'], data['content_title'])
        self.assertEqual(exercise['description'], data['description'])

        version = content.internal.internal.get_edit_version()
        self.assertIsNotNone(version)
        parent_topic = content.internal.topic._get_by_id(data['parent_id'], version)
        self.assertIsNotNone(parent_topic)
        self.assertEqual(parent_topic['child_keys'].count(exercise.key), 1)

        change = content.internal.exercise.get_content_change(exercise, version)
        changed_data = pickle.loads(change['content_changes'])
        self.assertIsNotNone(change)
        self.assertEqual(change['version'], version.key)
        self.assertEqual(change['content'], exercise.key)
        self.assertEqual(changed_data['pretty_display_name'], new_data['content_title'])
        self.assertEqual(changed_data['description'], new_data['description'])

    def test_update_fail(self):
        # Create test exercise.
        data = {
            'exercise_name': self.test_ex_name,
            'content_title': self.test_ex_title,
            'description': self.test_ex_desc,
            'parent_id': self.test_ex_parent_ids[0],
            'grade': self.test_ex_grade,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/exercise/create?validation_only=0&',
                       method='post', data=data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(resp_json['content_id'], data['exercise_name'])
        self.assertEqual(resp_json['errors'], [])

        # Update with invalid name.
        new_data = {
            'exercise_name': self.test_ex_name + '_unknown',
            'content_title': self.test_ex_title + ' (updated)',
            'description': self.test_ex_desc + '(updated)',
            'grade': self.test_ex_grade,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/exercise/update?validation_only=0&',
                       method='put', data=new_data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(resp_json['content_id'], new_data['exercise_name'])
        self.assertEqual(resp_json['errors'], ["這個代號的知識點不存在"])

        # Update with invalid title.
        new_data = {
            'exercise_name': self.test_ex_name,
            'content_title': '',
            'description': self.test_ex_desc + '(updated)',
            'grade': self.test_ex_grade,
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/exercise/update?validation_only=0&',
                       method='put', data=new_data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(resp_json['content_id'], new_data['exercise_name'])
        self.assertEqual(resp_json['errors'], ["標題不能空白"])

        # Invalid grade. Grade is not interger.
        new_data = {
            'exercise_name': self.test_ex_name,
            'content_title': self.test_ex_title + ' (updated)',
            'description': self.test_ex_desc + '(updated)',
            'grade': 'invalid_grade',
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/exercise/update?validation_only=0&',
                       method='put', data=new_data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(resp_json['content_id'], new_data['exercise_name'])
        self.assertEqual(resp_json['errors'], ["年級欄位為必填，且必須填寫 0 ~ 12 的整數"])

        # Invalid grade. Grade is out of range.
        new_data = {
            'exercise_name': self.test_ex_name,
            'content_title': self.test_ex_title + ' (updated)',
            'description': self.test_ex_desc + '(updated)',
            'grade': '13',
        }
        r = self.fetch('/api/content/topictree/edit/edu_sheet/exercise/update?validation_only=0&',
                       method='put', data=new_data)
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(resp_json['content_id'], new_data['exercise_name'])
        self.assertEqual(resp_json['errors'], ["年級必須填寫 0 ~ 12 的整數"])


class TestSection(TestMainBase):
    PARENT_ID = 'math'
    CREATE_TITLE = 'Custom Section Title'
    UPDATE_ID = 'updatedid'
    UPDATE_TITLE = 'Updated Custom Section Title'
    UPDATE_STANDALONE_TITLE = 'Updated Standalone Title'

    def setUp(self):
        super().setUp()
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True))
        self.created_id = None

    def tearDown(self):
        super().tearDown()
        if not self.created_id:
            return

        edit_version = content.internal.internal.get_edit_version()
        root_key = content.internal.topic.get_root_key(edit_version)
        self._get_by_section_id(self.created_id, edit_version, root_key)

        content.section.delete_section(self.created_id)

        entity = content.internal.topic._get_by_id(self.created_id, edit_version, root_key)
        retry_count = 0
        while entity and retry_count < 5:
            entity = content.internal.topic._get_by_id(self.created_id, edit_version, root_key)
            retry_count += 1

    def test_create_ok_without_id(self):
        # /devadmin/content 後台使用的 happy path
        request_data = {
            'parent_id': self.PARENT_ID,
        }
        edit_version = content.internal.internal.get_edit_version()
        expect_id = content.internal.topic.get_new_id(self.PARENT_ID, content.section.DEFAULT_TITLE, edit_version)

        r = self.fetch('/api/content/section', method='POST', data=request_data)

        self.assertEqual(r.status_code, 200, r.headers)
        self.assertEqual(r.headers['Content-Type'], 'application/json')
        resp_json = json.loads(r.data.decode('utf-8'))
        self.created_id = resp_json['id']
        self.assertEqual(resp_json['id'], expect_id)  # e.g. v353-new-section
        self.assertEqual(resp_json['title'], content.section.DEFAULT_TITLE)
        self.assertEqual(resp_json['standalone_title'], content.section.DEFAULT_TITLE)
        root_key = content.internal.topic.get_root_key(edit_version)
        created_section_entity = self._get_by_section_id(resp_json['id'], edit_version, root_key)
        self.assertIsNotNone(created_section_entity)
        self.assertEqual(created_section_entity['id'], resp_json['id'])
        self.assertEqual(created_section_entity['title'], resp_json['title'])
        self.assertEqual(created_section_entity['standalone_title'], resp_json['standalone_title'])
        self.assertTrue(created_section_entity['is_section'])

    def test_create_ok_with_id(self):
        # 批次上架使用的 happy path
        self.created_id = 'batch_create_section_id'
        request_data = {
            'id': self.created_id,
            'title': self.CREATE_TITLE,
            'parent_id': self.PARENT_ID,
        }
        edit_version = content.internal.internal.get_edit_version()
        root_key = content.internal.topic.get_root_key(edit_version)

        self.assertIsNone(content.internal.topic._get_by_id(self.created_id, edit_version, root_key))
        r = self.fetch('/api/content/section', method='POST', data=request_data)

        self.assertEqual(r.status_code, 201, r.headers)
        self.assertEqual(r.headers['Content-Type'], 'text/html; charset=utf-8')
        self.assertEqual(r.headers['Content-Length'], '0')
        created_section_entity = self._get_by_section_id(self.created_id, edit_version, root_key)
        self.assertIsNotNone(created_section_entity)
        self.assertEqual(created_section_entity['id'], self.created_id)
        self.assertEqual(created_section_entity['title'], self.CREATE_TITLE)
        self.assertEqual(created_section_entity['standalone_title'], self.CREATE_TITLE)
        self.assertTrue(created_section_entity['is_section'])

    def test_create_ok_with_id_without_title(self):
        # 沒有地方用，但合法的 query
        self.created_id = 'valid_custom_section_id'
        request_data = {
            'id': self.created_id,
            'parent_id': self.PARENT_ID,
        }
        edit_version = content.internal.internal.get_edit_version()
        root_key = content.internal.topic.get_root_key(edit_version)

        self.assertIsNone(content.internal.topic._get_by_id(self.created_id, edit_version, root_key))
        r = self.fetch('/api/content/section', method='POST', data=request_data)

        self.assertEqual(r.status_code, 201)
        self.assertEqual(r.headers['Content-Type'], 'text/html; charset=utf-8')
        self.assertEqual(r.headers['Content-Length'], '0')
        created_section_entity = self._get_by_section_id(self.created_id, edit_version, root_key)
        self.assertIsNotNone(created_section_entity)
        self.assertEqual(created_section_entity['id'], self.created_id)
        self.assertEqual(created_section_entity['title'], content.section.DEFAULT_TITLE)
        self.assertEqual(created_section_entity['standalone_title'], content.section.DEFAULT_TITLE)
        self.assertTrue(created_section_entity['is_section'])

    def test_create_fail(self):
        # 缺少 payload
        r = self.fetch('/api/content/section', method='POST')
        self.assertEqual(r.status_code, 415)

        # 缺少 parent_id
        r = self.fetch('/api/content/section', method='POST', data={'title': self.CREATE_TITLE})
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), 'Missing required parameter: parent_id')

        # 不合法的 id
        r = self.fetch('/api/content/section', method='POST', data={'id': 'a' * 1000,
                                                                    'title': '',
                                                                    'parent_id': 'xxx'})
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), '代號不能超過 500 個字, ' + 'a' * 1000)

        # 不合法的 id
        r = self.fetch('/api/content/section', method='POST', data={'id': 'FAKE_SECTION',
                                                                    'title': '',
                                                                    'parent_id': 'xxx'})
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), '代號 FAKE_SECTION 存在不合法字元')

        # 不合法的 title
        r = self.fetch('/api/content/section', method='POST', data={'id': 'fakesection',
                                                                    'title': 'a' * 200,
                                                                    'parent_id': 'xxx'})
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), '標題不能超過 100 個字, ' + 'a' * 200)

        # 不存在的 parent topic
        r = self.fetch('/api/content/section', method='POST', data={'id': 'fakesection',
                                                                    'title': 'fake title',
                                                                    'parent_id': 'xxx'})
        self.assertEqual(r.status_code, 404)
        self.assertEqual(r.data.decode('utf-8'), '預計擺放的母資料夾 [xxx] 不存在')

        # section 下 create section
        self.create_section()
        r = self.fetch('/api/content/section', method='POST', data={'id': 'fakesection',
                                                                    'title': 'fake title',
                                                                    'parent_id': self.created_id})
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), '不合法的操作：小節下不能建立主題或小節\n'
                                                 'Details: parent id [%s], new section id [fakesection]'
                         % self.created_id)

    def test_create_fail_conflict(self):
        self.create_section()

        # 再建一次同 id 的 section
        request_data = {
            'id': self.created_id,
            'title': 'Conflict custom section title',
            'parent_id': self.PARENT_ID,
        }
        r = self.fetch('/api/content/section', method='POST', data=request_data)
        self.assertEqual(r.status_code, 409)
        self.assertEqual(r.data.decode('utf-8'),
                         '已存在代號是 batch_create_section_id 的資料夾，該資料夾的標題是 [Custom Section Title]')

        # 再建一次同 id 的 section, 這次換一個 parent_id，一樣要失敗
        request_data['parent_id'] = 'khan-fractions'
        r = self.fetch('/api/content/section', method='POST', data=request_data)
        self.assertEqual(r.status_code, 409)
        self.assertEqual(r.data.decode('utf-8'),
                         '已存在代號是 batch_create_section_id 的資料夾，該資料夾的標題是 [Custom Section Title]')

    def test_read_edit_version_children_ok(self):
        edit_version, root_key, _ = self.create_section()

        # 讀到正確的資料，此時 children 為空
        r = self.fetch('/api/content/topicversion/edit/section/' + self.created_id, method='GET')
        self.assertEqual(r.status_code, 200, r.headers)
        self.assertFalse('Cache-Control' in r.headers)
        data = json.loads(r.data.decode('utf-8'))
        self.assertEqual(data['kind'], 'Section')
        self.assertEqual(data['id'], self.created_id)
        self.assertEqual(data['standalone_title'], self.CREATE_TITLE)
        self.assertEqual(data['title'], self.CREATE_TITLE)
        self.assertEqual(data['children'], [])

        # hack 進去加兩個 children
        content.section.add_children(section_id=self.created_id,
                                     child_keys=[datastore.Key('Exercise', 55879, project='junyiacademy'),
                                                 datastore.Key('Video', 11791172, project='junyiacademy')])

        # work around, 避開 eventually consistency 的問題
        self._get_by_section_id(self.created_id, edit_version, root_key, check_fn=lambda e: e and 'child_keys' in e)

        # 讀到正確的資料，此時 children 有兩筆資料
        r = self.fetch('/api/content/topicversion/edit/section/' + self.created_id, method='GET')
        self.assertEqual(r.status_code, 200, r.headers)
        self.assertFalse('Cache-Control' in r.headers)
        data = json.loads(r.data.decode('utf-8'))
        self.assertEqual(data['kind'], 'Section')
        self.assertEqual(data['id'], self.created_id)
        self.assertEqual(data['standalone_title'], self.CREATE_TITLE)
        self.assertEqual(data['title'], self.CREATE_TITLE)
        self.assertEqual(len(data['children']), 2)
        self.assertEqual(data['children'][0]['kind'], 'Exercise')
        self.assertEqual(data['children'][0]['id'], 'representing_numbers')
        self.assertEqual(data['children'][0]['title'], '數一數')
        self.assertEqual(data['children'][1]['kind'], 'Video')
        self.assertEqual(data['children'][1]['id'], 'Tv8Velc93mE')
        self.assertEqual(data['children'][1]['title'], '三位數的加法與減法1')

    @patch('content.section.read_section')
    def test_read_default_version_ok(self, read_section_patch):
        is_published = True
        read_section_patch.return_value = is_published, {'id': 'fake_section'}

        # 驗證 cache control header
        # 用數字版號 query, 不一定是 default version, 也可能是古早的 topic version
        r = self.fetch('/api/content/topicversion/300/section/FAKE_SECTION', method='GET')
        self.assertEqual(r.status_code, 200)
        self.assertTrue('Cache-Control' in r.headers, r.headers)
        self.assertEqual(r.headers['Cache-Control'], 'max-age=%d' % main.FIX_CONTENT_MAX_CACHE_AGE)
        # 用 'default' alias query
        r = self.fetch('/api/content/topicversion/default/section/FAKE_SECTION', method='GET')
        self.assertEqual(r.status_code, 200)
        self.assertFalse('Cache-Control' in r.headers, r.headers)

    @patch('content.section.read_section')
    def test_read_edit_version_ok(self, read_section_patch):
        is_published = False
        read_section_patch.return_value = is_published, {'id': 'fake_section'}

        # 驗證 cache control header
        # 用數字版號 query
        r = self.fetch('/api/content/topicversion/300/section/FAKE_SECTION', method='GET')
        self.assertEqual(r.status_code, 200)
        self.assertFalse('Cache-Control' in r.headers, r.headers)
        # 用 'edit' alias query
        r = self.fetch('/api/content/topicversion/edit/section/FAKE_SECTION', method='GET')
        self.assertEqual(r.status_code, 200)
        self.assertFalse('Cache-Control' in r.headers, r.headers)

    def test_read_fail(self):
        # topic version id 不合法
        r = self.fetch('/api/content/topicversion/X/section/SECTION_ID', method='GET')
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), 'Invalid version_id. [X]')

        # edit version 不存在此 section
        r = self.fetch('/api/content/topicversion/edit/section/NO_SUCH_SECTION_ID', method='GET')
        self.assertEqual(r.status_code, 404)
        self.assertEqual(r.data.decode('utf-8'), 'No Section for section_id [NO_SUCH_SECTION_ID] in version [edit]')

    def verify_update_section(self, update_request_payload):
        def get_property(prop_name):
            return update_request_payload.get(prop_name) or created_section_entity.get(prop_name)

        def check_fn(entity):
            if not entity:
                return False
            for update_prop in update_request_payload.keys():
                if update_prop not in entity:
                    return False
                if entity[update_prop] != update_request_payload[update_prop]:
                    return False
            return True

        edit_version, root_key, created_section_entity = self.create_section()
        r = self.fetch('/api/content/section/' + created_section_entity['id'], method='PUT', data=update_request_payload)

        self.assertEqual(r.status_code, 204, r.headers)
        if 'id' in update_request_payload:
            self.created_id = update_request_payload['id']
        self.assertEqual(r.headers['Content-Type'], 'text/html; charset=utf-8')
        updated_section_entity = self._get_by_section_id(self.created_id, edit_version, root_key, check_fn=check_fn)
        self.assertIsNotNone(updated_section_entity)
        self.assertEqual(updated_section_entity['id'], self.created_id)
        self.assertEqual(updated_section_entity['title'], get_property('title'))
        self.assertEqual(updated_section_entity['standalone_title'], get_property('standalone_title'))
        self.assertTrue(updated_section_entity['is_section'])

    def test_update_ok(self):
        request_data = {'id': self.UPDATE_ID,
                        'title': self.UPDATE_TITLE,
                        'standalone_title': self.UPDATE_STANDALONE_TITLE}
        self.verify_update_section(request_data)

    def test_update_nothing_ok(self):
        self.verify_update_section({})

    def test_update_id_ok(self):
        self.verify_update_section({'id': self.UPDATE_ID})

    def test_update_title_ok(self):
        self.verify_update_section({'title': self.UPDATE_TITLE})

    def test_update_standalone_title_ok(self):
        self.verify_update_section({'standalone_title': self.UPDATE_STANDALONE_TITLE})

    def test_update_fail(self):
        # 缺少 payload
        r = self.fetch('/api/content/section/FAKE_SECTION_ID', method='PUT')
        self.assertEqual(r.status_code, 415, r.headers)

        # 不存在此 section
        request_data = {'title': 'x'}
        r = self.fetch('/api/content/section/no_such_section', method='PUT', data=request_data)
        self.assertEqual(r.status_code, 404, r.headers)
        self.assertEqual(r.data.decode('utf-8'), '這個代號的資料夾不存在 no_such_section')

        # id 不合法
        request_data['id'] = ''
        r = self.fetch('/api/content/section/FAKE_SECTION_ID', method='PUT', data=request_data)
        self.assertEqual(r.status_code, 400, r.headers)
        self.assertEqual(r.data.decode('utf-8'), '資料夾的代號不能空白')

        # title 不合法
        request_data['title'] = ''
        r = self.fetch('/api/content/section/FAKE_SECTION_ID', method='PUT', data=request_data)
        self.assertEqual(r.status_code, 400, r.headers)
        self.assertEqual(r.data.decode('utf-8'), '資料夾的標題不能空白')

        # id conflict
        self.create_section()
        request_data['id'] = self.PARENT_ID
        request_data['title'] = 'new title'
        r = self.fetch('/api/content/section/' + self.created_id, method='PUT', data=request_data)
        self.assertEqual(r.status_code, 409, r.headers)
        self.assertEqual(r.data.decode('utf-8'), '已存在代號是 math 的資料夾，該資料夾的標題是 [數學 (英語發音)]')

    def create_section(self):
        # 建立 section
        self.created_id = 'batch_create_section_id'
        request_data = {
            'id': self.created_id,
            'title': self.CREATE_TITLE,
            'parent_id': self.PARENT_ID,
        }
        edit_version = content.internal.internal.get_edit_version()
        root_key = content.internal.topic.get_root_key(edit_version)
        self.assertIsNone(content.internal.topic._get_by_id(self.created_id, edit_version, root_key))
        r = self.fetch('/api/content/section', method='POST', data=request_data)
        self.assertEqual(r.status_code, 201, r.headers)
        created_section_entity = self._get_by_section_id(self.created_id, edit_version, root_key)
        self.assertIsNotNone(created_section_entity)
        return edit_version, root_key, created_section_entity

    @staticmethod
    def _get_by_section_id(section_id, version, root_key, check_fn=None):
        if check_fn is None:
            check_fn = lambda e: e is not None
        retry_count = 0
        entity = None
        while not check_fn(entity) and retry_count < 5:
            entity = content.internal.topic._get_by_id(section_id, version, root_key)
            retry_count += 1
        return entity


class TestVideoEduSheet(TestMainBase):
    readable_id = 'NehkLV77ITk'
    parent_id = 'math'
    grade = '3'
    db_changed_create = False

    def setUp(self):
        super().setUp()
        self.cleanUpVideoEntity()
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True))

    def tearDown(self):
        if not self.db_changed_create:
            return

        self.cleanUpVideoEntity()

    def cleanUpVideoEntity(self):
        video_entity = content.internal.video.get_for_readable_id(self.readable_id)
        if video_entity is None:
            return

        client = content.internal.internal.get_client()
        topic_entity = content.internal.topic._get_by_id(self.parent_id, content.internal.internal.get_edit_version())
        if video_entity.key in topic_entity['child_keys']:
            topic_entity['child_keys'].remove(video_entity.key)
        client.put(topic_entity)
        client.delete(video_entity.key)

    def get_mock_data(self):
        return {
            'readable_id': self.readable_id,
            'content_title': 'title',
            'description': 'description',
          'parent_id': self.parent_id,
            'grade': self.grade,
        }

    def check_db_record(self, description):
        video_entity = content.internal.video.get_for_readable_id(self.readable_id)
        version = content.internal.internal.get_version('edit')
        existing_change = content.internal.video.get_content_change(video_entity, version)
        if existing_change:
            video_entity.update(pickle.loads(existing_change['content_changes']))

        self.assertIsNotNone(video_entity)
        self.assertEqual(video_entity['description'], description)

        for _ in range(5):
            try:
                topic_entity = content.internal.topic._get_by_id(self.parent_id, content.internal.internal.get_edit_version())
                self.assertTrue(video_entity.key in topic_entity['child_keys'])
                break
            except AssertionError as e:
                pass
        else:
            self.assertTrue(False)

    def test_default_version(self):
        data = self.get_mock_data()

        r = self.fetch('/api/content/topictree/default/edu_sheet/video/create?validation_only=0&',
                       method='post', data=data)
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), '目前禁止於 edit 以外的版本建立 Video')

        r = self.fetch('/api/content/topictree/default/edu_sheet/video/update?validation_only=0&',
                       method='put', data=data)
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), '目前禁止於 edit 以外的版本修改 Video')

    def test_create_fail(self):
        data = self.get_mock_data()
        data['content_title'] = 'a' * 101

        r = self.fetch('/api/content/topictree/edit/edu_sheet/video/create?validation_only=0&',
                       method='post', data=data)
        resp_json = json.loads(r.data.decode('utf-8'))

        self.assertEqual(r.status_code, 200)
        self.assertListEqual(resp_json['errors'], ['標題不能超過 100 個字: %s' % data['content_title']])

    def test_create_update_ok(self):
        self.db_changed_create = True
        data = self.get_mock_data()

        # Create first
        r = self.fetch('/api/content/topictree/edit/edu_sheet/video/create?validation_only=0&',
                       method='post', data=data)
        self.assertEqual(r.status_code, 200)

        # Update the same entity and check
        data['description'] = 'new description'
        r = self.fetch('/api/content/topictree/edit/edu_sheet/video/update?validation_only=0&',
                       method='put', data=data)
        resp_json = json.loads(r.data.decode('utf-8'))

        self.assertEqual(r.status_code, 200)
        self.assertListEqual(resp_json['errors'], [])
        self.check_db_record(data['description'])

    def test_update_fail(self):
        data = self.get_mock_data()
        data['readable_id'] = 'non_exist_readable_id'

        r = self.fetch('/api/content/topictree/edit/edu_sheet/video/update?validation_only=0&',
                       method='put', data=data)
        resp_json = json.loads(r.data.decode('utf-8'))

        self.assertEqual(r.status_code, 200)
        self.assertListEqual(resp_json['errors'], ['這個代號的影片不存在'])

    # TODO test create existed video in different topic


class TestVideoPlayData(TestMainBase):
    TEST_TOPIC_ID = 'basic-exponents'
    TEST_VIDEO_ID = 'level-1-exponents'
    TEST_VIDEO_TITLE = '等級1 指數 (英)'

    def setUp(self):
        super().setUp()
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True))

    def test_invalid_params(self):
        r = self.fetch('/api/content/videos/play')
        self.assertEqual(r.status_code, 400)
        self.assertIn('Require [topic_id] GET param.', str(r.data))

        r = self.fetch('/api/content/videos/play?'
                       f'video_id={self.TEST_VIDEO_ID}')
        self.assertEqual(r.status_code, 400)
        self.assertIn('Require [topic_id] GET param.', str(r.data))

        r = self.fetch('/api/content/videos/play?'
                       f'topic_id={self.TEST_TOPIC_ID}')
        self.assertEqual(r.status_code, 400)
        self.assertIn('Require [video_id] GET param.', str(r.data))

        r = self.fetch('/api/content/videos/play?'
                       f'topic_id=not_exist_topic&'
                       f'video_id={self.TEST_VIDEO_ID}')
        self.assertEqual(r.status_code, 400)
        self.assertIn('No Topic for topic_id [not_exist_topic] in '
                      'default version', str(r.data))

        r = self.fetch('/api/content/videos/play?'
                       f'topic_id={self.TEST_TOPIC_ID}&'
                       f'video_id=not_exist_video')
        self.assertEqual(r.status_code, 400)
        self.assertIn('no such video_id [not_exist_video]', str(r.data))

    def test_ok(self):
        r = self.fetch('/api/content/videos/play?'
                       f'topic_id={self.TEST_TOPIC_ID}&'
                       f'video_id={self.TEST_VIDEO_ID}')
        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(r.status_code, 200)
        self.assertIsNotNone(resp_json)
        self.assertEqual(resp_json['video']['title'], self.TEST_VIDEO_TITLE)


class TestVideoGet(TestMainBase):
    TEST_VIDEO_ID = 'level-1-exponents'
    TEST_VERSION_ID = 'edit'

    def _assert_output_field(self, data: dict):
        for field in ['access_control', 'backup_timestamp', 'date_added',
                      'description', 'duration', 'ka_url', 'keywords', 'kind',
                      'readable_id', 'relative_url', 'subject_list', 'title',
                      'topic_string_keys', 'url', 'views', 'youtube_id']:
            self.assertIn(field, data)

    def test_unauthorized(self):
        self.client.set_cookie('localhost', 'JAID', '')
        r = self.fetch(f'/api/content/v1/videos/{self.TEST_VIDEO_ID}')
        self.assertEqual(r.status_code, 401)

    def test_no_content_found(self):
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True))
        r = self.fetch(f'/api/content/v1/videos/some-invalid-video-readable-id')
        self.assertEqual(r.status_code, 404)

    def test_ok(self):
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True))
        r = self.fetch(f'/api/content/v1/topicversion/{self.TEST_VERSION_ID}'
                       f'/videos/{self.TEST_VIDEO_ID}')
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data.decode('utf-8'))
        self._assert_output_field(data)

    def test_get_video_without_version(self):
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True))
        r = self.fetch(f'/api/content/v1/videos/{self.TEST_VIDEO_ID}')
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data.decode('utf-8'))
        self._assert_output_field(data)
        

class TestVideoGetAllE2E(TestMainBase):

    def test_video_get_all(self):
        r = self.fetch('/api/content/videos')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.headers['Content-Type'], 'application/json')
        data = json.loads(r.data.decode('utf-8'))
        self.assertIsNotNone(data)
        # Check the first video to see that it has the expected data keys
        self.assertTrue('display_name' in data[0])
        self.assertTrue('readable_id' in data[0])
