from base_cache import BaseCache


class MockCache(BaseCache):
    in_mem_cache = {}

    @classmethod
    def _get_value(cls, key):
        return cls.in_mem_cache.get(key, None)

    @classmethod
    def _set_value(cls, key, value, time):
        # TODO: arg time does not work
        cls.in_mem_cache[key] = value

    @classmethod
    def _delete_value(cls, key):
        del cls.in_mem_cache[key]

    @classmethod
    def flush(cls):
        cls.in_mem_cache.clear()
