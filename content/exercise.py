# -*- coding: utf-8 -*-
import service_cache
from content import exception
from content.internal import exercise, internal


def gen_exercise_get_all_name_cache_key(*args, **kwargs):
    return "exercise_get_all_name_{}_{}_{}".format(
        kwargs.get('live_only', 'True'),
        kwargs.get('include_exam', 'False'),
        internal.get_setting("cached_exercises_date")
    )


@service_cache.cache_with_key_fxn(gen_exercise_get_all_name_cache_key)
def exercise_get_all_name(live_only=True, include_exam=False):
    exercises = exercise.Exercise.get_all_use_cache(
        live_only=live_only, include_exam=include_exam
    )
    ex_data = []

    for e in exercises:
        ex_ele = {}
        ex_ele['name'] = e.name
        ex_ele['display_name'] = e.pretty_display_name
        ex_data.append(ex_ele)
    return ex_data


def create_exercise(exercise_name, content_title, description, parent_id, grade, validation_only):
    errors = _validate_exercise_required_info_by_edu_sheet(exercise_name, content_title, grade)
    if errors:
        return errors

    if validation_only:
        try:
            exercise.validate_consistency(exercise_name, content_title, description, parent_id, int(grade))
        except exception.ExerciseCreateError as e:
            errors.append(str(e))
        return errors

    try:
        exercise.create_exercise(exercise_name, content_title, description, parent_id, int(grade))
    except exception.ExerciseCreateError as e:
        errors.append(str(e))

    return errors


def update_exercise(exercise_name, content_title, description, grade, validation_only):
    errors = _validate_exercise_required_info_by_edu_sheet(exercise_name, content_title, grade)
    if errors or validation_only:
        return errors

    try:
        exercise.update_exercise(exercise_name, content_title, description, int(grade))
    except exception.ExerciseUpdateError as e:
        errors.append(str(e))

    return errors


def _validate_exercise_required_info_by_edu_sheet(exercise_name, content_title, grade):
    errors = []

    try:
        exercise.validate_name_format(exercise_name)
    except exception.InvalidFormat as e:
        errors.append(str(e))

    try:
        exercise.validate_title_format(content_title)
    except exception.InvalidFormat as e:
        errors.append(str(e))

    try:
        int_grade = int(grade)
    except ValueError:
        errors.append("年級欄位為必填，且必須填寫 0 ~ 12 的整數")
    else:
        if (int_grade > 12 or int_grade < 0):
            errors.append("年級必須填寫 0 ~ 12 的整數")


    try:
        internal.get_edit_version()
    except internal.InvalidContentTree:
        errors.append("嚴重錯誤：edit version 不存在")

    return errors
