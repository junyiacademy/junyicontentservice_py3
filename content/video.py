# -*- coding: utf-8 -*-
from copy import copy
import pickle

from . import exception
from . import helpers
from .internal import internal
from .internal import socrates_question
from .internal import topic
from .internal import video as video_lib
from .internal import youtube_sync


def validate_video_required_info_by_edu_sheet(readable_id, content_title, version_id):
    errors = []

    if version_id != 'edit':
        errors.append("不允許編輯此版本 [%s]" % version_id)

    if not readable_id:
        errors.append("代號不能空白")
    elif len(readable_id) > 500:
        errors.append("代號不能超過 500 個字: %s" % readable_id)

    if not content_title:
        errors.append("標題不能空白")
    elif len(content_title) > 100:
        errors.append("標題不能超過 100 個字: %s" % content_title)

    try:
        internal.get_edit_version()
    except internal.InvalidContentTree:
        errors.append("嚴重錯誤：edit version 不存在")

    return errors

def create_video(readable_id, content_title, description, parent_id, grade, validation_only, version_id):
    errors = validate_video_required_info_by_edu_sheet(readable_id, content_title, version_id)
    if errors:
        return errors

    version = internal.get_version(version_id) 
    parent_topic = topic._get_by_id(parent_id, version)

    if not parent_topic:
        return ["預計擺放的母資料夾[%s]不存在" % parent_id]

    try:
        int_grade = int(grade)
    except ValueError:
        return ["年級欄位為必填，且必須填寫 0 ~ 12 的整數"]

    if int_grade > 12 or int_grade < 0:
        return ["年級必須填寫 0 ~ 12 的整數"]


    video_entity = video_lib.get_for_readable_id(readable_id)
    new_video_data = {}
    if video_entity:
        video_without_change_applied = copy(video_entity)

        existing_change = video_lib.get_content_change(video_entity, version)
        if existing_change:
            new_video_data = pickle.loads(existing_change['content_changes'])
            video_entity.update(new_video_data)

        if 'child_keys' in parent_topic and video_entity.key in parent_topic['child_keys']:
            return ["母資料夾中，已存在相同代號的影片[%s]" % video_entity['title']]
        elif content_title != video_entity['title']:
            return ["此影片已存在，且已存在的影片標題[%s]和預計上架的標題不一樣" % video_entity['title']]
        elif int_grade != video_entity['grade']:
            return ["此影片已存在，且已存在的影片的年級和預計上架的年級不一樣"]
        elif readable_id != video_entity['youtube_id']:
            return ["此影片已存在，且已存在的影片 youtube_id[%s] 和預計上架的 youtube_id 不一樣" % video_entity['youtube_id']]

        video_entity = video_without_change_applied
    else:
        new_video_data = youtube_sync.youtube_get_video_data_dict(readable_id)
        if not new_video_data:
            return ["這個代號的影片尚未上架到 youtube 頻道"]

    if validation_only:
        return errors

    new_video_data["title"] = content_title
    new_video_data["description"] = description
    # 更新 topic_string_keys
    new_video_data["topic_string_keys"] = helpers.get_updated_topic_string_keys(parent_topic, video_entity)
    new_video_data["grade"] = int_grade

    if video_entity:
        video_lib.add_content_change(video_entity, version, new_video_data)
        topic.add_child(video_entity, parent_id, version)
    else:
        try:
            created_video = video_lib.create_video(
                readable_id=readable_id,
                video_data=new_video_data)
            topic.add_child(created_video, parent_id, version)
        except Exception as e:
            errors.append('建立影片失敗, 原因[ %s]' % str(e))

    return errors


def get_video(version_id: str,
              readable_id: str,
              is_applying_change: bool = True,
              is_live_only: bool = False) -> dict:
    video = video_lib.Video.from_readable_id(readable_id,
                                             version_id,
                                             is_applying_change=is_applying_change,
                                             is_live_only=is_live_only)
    return video.dump()


def update_video(readable_id, content_title, description, grade, validation_only, version_id):
    errors = validate_video_required_info_by_edu_sheet(readable_id, content_title, version_id)
    if errors:
        return errors

    video_entity = video_lib.get_for_readable_id(readable_id)
    if not video_entity:
        return ["這個代號的影片不存在"]
    
    try:
        int_grade = int(grade)
    except ValueError:
        return ["年級欄位為必填，且必須填寫 0 ~ 12 的整數"]

    if int_grade > 12 or int_grade < 0:
        return ["年級必須填寫 0 ~ 12 的整數"]

    if validation_only:
        return errors

    new_video_data = {}
    version = internal.get_version(version_id)

    new_video_data["title"] = content_title
    new_video_data["description"] = description
    new_video_data["grade"] = int_grade

    video_lib.add_content_change(video_entity, version, new_video_data)
    return []


def get_video_play(topic_id, video_id):
    parent_topic = topic.Topic.from_id(topic_id=topic_id)
    video_entities_list = parent_topic.get_children_entities_of_kind('Video')

    video_data, video = video_lib.get_play_data(parent_topic,
                                                video_entities_list,
                                                video_id)
    if video_data is None:
        raise exception.ContentNotExistsError(f'no such video_id [{video_id}]')

    video_data.pop('topic_string_keys', None)

    video_data["socrates_enabled"] = socrates_question.SocratesQuestion.exists_for_video(
        video_data["readable_id"])

    # # checking access control
    # video_data["access_checking"] = "authorized"
    # if video.access_control == "login_required":
    #   user_data = user_models.UserData.current()
    #   if not user_data or user_data.is_phantom or user_data.is_pre_phantom:
    #     video_data["access_checking"] = "login_required"

    return video_data


def get_all_videos():
    return video_lib.get_all()
