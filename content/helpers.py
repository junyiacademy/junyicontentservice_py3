from typing import Optional, List, Union
from google.cloud import datastore
from content.internal.utils import get_legacy_key_str


def get_updated_topic_string_keys(parent_topic: datastore.Entity, content: Optional[datastore.Entity]) -> Union[List[str], str]:
    '''
    因為 TSV 實做原因，只有一個值時，property 格式需為字串。有多個值時，回傳格式需為 list
    '''
    key: str = get_legacy_key_str(parent_topic)

    if not content:
        return key
    topic_string_keys: List[str] = content['topic_string_keys'].split('\t')
    if key not in topic_string_keys:
        topic_string_keys.append(key)

    return topic_string_keys
