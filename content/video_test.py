# -*- coding: utf-8 -*-
import unittest
from unittest import mock
from datetime import datetime, timezone

from . import video as video_lib
from content.internal import mock_entity, video as internal_video_lib
from content.internal.internal import InvalidContentTree


class TestValidateVideoRequiredInfoByEduSheet(unittest.TestCase):
    def test_ok(self):
        errors = video_lib.validate_video_required_info_by_edu_sheet('a' * 500, 'a' * 100, 'edit')
        self.assertListEqual(errors, [])

    def test_invalid_args(self):
        long_id = 'a' * 501
        long_title = 'a' * 101
        errors = video_lib.validate_video_required_info_by_edu_sheet(long_id, long_title, 'edit')
        self.assertListEqual(errors, ["代號不能超過 500 個字: %s" % long_id,
                                      "標題不能超過 100 個字: %s" % long_title])

        errors = video_lib.validate_video_required_info_by_edu_sheet('', '', 'invalid_edit')
        self.assertListEqual(errors, ["不允許編輯此版本 [invalid_edit]",
                                      "代號不能空白",
                                      "標題不能空白"])

    @mock.patch('content.internal.internal.get_edit_version')
    def test_no_edit_version(self, get_edit_version_patch):
        get_edit_version_patch.side_effect = InvalidContentTree

        errors = video_lib.validate_video_required_info_by_edu_sheet('id', 'title', 'edit')
        self.assertListEqual(errors, ["嚴重錯誤：edit version 不存在"])


class TestCreateVideo(unittest.TestCase):
    @mock.patch('content.internal.video.create_video')
    @mock.patch('content.internal.youtube_sync.youtube_get_video_data_dict')
    def test_validation_only_ok(self, youtube_get_video_data_dict_patch, create_video_patch):
        errors = video_lib.create_video(
            'readable_id', 'title', 'description', 'math', 3, True, 'edit')
        self.assertListEqual(errors, [])
        create_video_patch.assert_not_called()

    @mock.patch('content.video.validate_video_required_info_by_edu_sheet')
    def test_validate_video_required_info_by_edu_sheet_called(
        self, validate_video_required_info_by_edu_sheet_patch):
        validate_video_required_info_by_edu_sheet_patch.return_value = ['error']

        errors = video_lib.create_video(
            'readable_id', 'title', 'description', 'parent_id', 3, True, 'edit')
        validate_video_required_info_by_edu_sheet_patch.assert_called_once_with(
            'readable_id', 'title', 'edit')
        self.assertListEqual(errors, ['error'])

    def test_non_exist_parent_topic(self):
        errors = video_lib.create_video(
            'readable_id', 'title', 'description', 'non_exist_parent_id', 3, True, 'edit')
        self.assertListEqual(errors, ['預計擺放的母資料夾[non_exist_parent_id]不存在'])

    def test_grade_is_not_interger(self):
        errors = video_lib.create_video(
            'readable_id', 'title', 'description', 'math', 'invalid_grade', True, 'edit')
        self.assertListEqual(errors, ['年級欄位為必填，且必須填寫 0 ~ 12 的整數'])

    def test_grade_out_of_range(self):
        errors = video_lib.create_video(
            'readable_id', 'title', 'description', 'math', 13, True, 'edit')
        self.assertListEqual(errors, ['年級必須填寫 0 ~ 12 的整數'])

    def test_video_not_upload(self):
        errors = video_lib.create_video(
            'readable_id', 'title', 'description', 'math', 3, True, 'edit')
        self.assertListEqual(errors, ['這個代號的影片尚未上架到 youtube 頻道'])

    @mock.patch('content.internal.topic._get_by_id')
    @mock.patch('content.internal.video.get_for_readable_id')
    def test_video_exist(self, get_for_readable_id_patch, get_by_id_patch):
        mock_video_entity = mock_entity.get_video_entity()
        get_for_readable_id_patch.return_value = mock_video_entity
        mock_parent_topic, _, _ = mock_entity.get_topic_entity()

        mock_parent_topic['child_keys'] = [mock_video_entity.key]
        get_by_id_patch.return_value = mock_parent_topic
        errors = video_lib.create_video(
            'readable_id', 'title', 'description', 'parent_id', 3, True, 'edit')
        self.assertListEqual(
                errors,
                ['母資料夾中，已存在相同代號的影片[{}]'.format(mock_video_entity['title'])])

        mock_parent_topic['child_keys'] = ['other_entity_key']
        get_by_id_patch.return_value = mock_parent_topic
        errors = video_lib.create_video(
            'readable_id', 'title', 'description', 'parent_id', 3, True, 'edit')
        self.assertListEqual(
            errors,
            ['此影片已存在，且已存在的影片標題[{}]和預計上架的標題不一樣'.format(
                    mock_video_entity['title'])])

        different_grade = 1
        errors = video_lib.create_video(
            'readable_id', mock_video_entity['title'], 'description', 'parent_id', different_grade, True, 'edit')
        self.assertListEqual(
            errors,
            ['此影片已存在，且已存在的影片的年級和預計上架的年級不一樣'])

        errors = video_lib.create_video(
            'readable_id', mock_video_entity['title'], 'description', 'parent_id', 3, True, 'edit')
        self.assertListEqual(
            errors,
            ['此影片已存在，且已存在的影片 youtube_id[{}] 和預計上架的 youtube_id 不一樣'.format(
                    mock_video_entity['youtube_id'])])

    @mock.patch('content.internal.topic.add_child')
    @mock.patch('content.internal.topic._get_by_id')
    @mock.patch('content.internal.video.create_video')
    @mock.patch('content.internal.youtube_sync.youtube_get_video_data_dict')
    def test_create_new_video_entity_ok(self, youtube_get_video_data_dict_patch, create_video_patch,
                                        get_by_id_patch, add_child_patch):
        errors = video_lib.create_video(
            'readable_id', 'title', 'description', 'math', 3, False, 'edit')
        self.assertListEqual(errors, [])
        create_video_patch.assert_called_once_with(readable_id='readable_id', video_data=mock.ANY)
        add_child_patch.assert_called_once_with(create_video_patch.return_value,
                'math', mock.ANY)


class TestUpdateVideo(unittest.TestCase):
    @mock.patch('content.internal.video.get_for_readable_id')
    @mock.patch('content.internal.video.add_content_change')
    def test_validation_only_ok(self, add_content_change_patch, get_for_readable_id_patch):
        errors = video_lib.update_video(
            'readable_id', 'title', 'description', 3, True, 'edit')
        self.assertListEqual(errors, [])
        add_content_change_patch.assert_not_called()

    @mock.patch('content.video.validate_video_required_info_by_edu_sheet')
    def test_validate_video_required_info_by_edu_sheet_called(
        self, validate_video_required_info_by_edu_sheet_patch):
        validate_video_required_info_by_edu_sheet_patch.return_value = ['error']

        errors = video_lib.update_video(
            'readable_id', 'title', 'description', 3, True, 'edit')
        validate_video_required_info_by_edu_sheet_patch.assert_called_once_with(
            'readable_id', 'title', 'edit')
        self.assertListEqual(errors, ['error'])

    def test_validation_only_fail(self):
        errors = video_lib.update_video(
            'non_exist_readable_id', 'title', 'description', 3, True, 'edit')
        self.assertListEqual(errors, ["這個代號的影片不存在"])

    @mock.patch('content.internal.video.get_for_readable_id')
    def test_grade_is_not_interger(self, get_for_readable_id_patch):
        errors = video_lib.update_video(
            'readable_id', 'title', 'description', 'invalid_grade', True, 'edit')
        self.assertListEqual(errors, ['年級欄位為必填，且必須填寫 0 ~ 12 的整數'])

    @mock.patch('content.internal.video.get_for_readable_id')
    def test_grade_out_of_range(self, get_for_readable_id_patch):
        errors = video_lib.update_video(
            'readable_id', 'title', 'description', 13, True, 'edit')
        self.assertListEqual(errors, ['年級必須填寫 0 ~ 12 的整數'])

    @mock.patch('content.internal.video.get_for_readable_id')
    @mock.patch('content.internal.video.get_content_change')
    @mock.patch('content.internal.video.add_content_change')
    def test_update_video_ok(self, add_content_change_patch, get_content_change_patch,
            get_for_readable_id_patch):
        get_content_change_patch.return_value = None
        errors = video_lib.update_video(
            'readable_id', 'title', 'description', 3, False, 'edit')
        self.assertListEqual(errors, [])
        add_content_change_patch.assert_called_once()


class TestGetVideo(unittest.TestCase):
    @mock.patch('content.internal.video.Video.from_readable_id')
    def test_get_video(self, video_from_readable_id_patch):
        mock_video_entity = mock_entity.get_video_entity()
        mock_video = internal_video_lib.Video(mock_video_entity)
        video_from_readable_id_patch.return_value = mock_video

        data = video_lib.get_video(version_id='', readable_id='', is_applying_change=True, is_live_only=False)
        self.assertEqual({
            'youtube_id': mock_video.youtube_id, 
            'url': mock_video.url, 
            'title': mock_video.title, 
            'titles_in_topic': mock_video.titles_in_topic, 
            'description': mock_video.description, 
            'keywords': mock_video.keywords,
            'grade': mock_video.grade,
            'duration': mock_video.duration,
            'readable_id': mock_video.readable_id, 
            'topic_string_keys': mock_video.topic_string_keys, 
            'views': mock_video.views, 
            'subject_list': mock_video.subject_list, 
            'access_control': mock_video.access_control,
            'date_added': mock_video.date_added,
            'backup_timestamp': mock_video.backup_timestamp, 
            'kind': 'Video', 
            'relative_url': mock_video.relative_url, 
            'ka_url': mock_video.ka_url,
            'id': mock_video.readable_id,
            'is_live': True,
            'is_hidden': None
        }, data)


class TestGetVideoPlay(unittest.TestCase):
    TEST_TOPIC_ID = 'basic-exponents'
    TEST_VIDEO_ID = 'level-1-exponents'

    def test_get_video_play_ok(self):
        video_data = video_lib.get_video_play(topic_id=self.TEST_TOPIC_ID,
                                         video_id=self.TEST_VIDEO_ID)
        self.assertLessEqual({
            'previous_video': {
                'key_id': 57822,
                'readable_id':'understanding-exponents-2',
                'title': '認識指數 2 (英)',
            },
            'next_video': {
                'key_id': 55758,
                'readable_id': 'level-2-exponents',
                'title': '等級2 指數 (英)',
            },
            'key': 'ahBkZXZ-anVueWlhY2FkZW15cg0LEgVWaWRlbxjNuwMM',
            'youtube_id': '8htcZca0JIA',
            'url': 'http://www.youtube.com/watch?v=8htcZca0JIA&feature=youtube_gdata_player',
            'title': '等級1 指數 (英)',
            'description': '基本的指數',
            'keywords': 'Math, Exponents, 指數',
            'duration': 587,
            'readable_id': self.TEST_VIDEO_ID,
            'canonical_url': f'/khan-videos/{self.TEST_TOPIC_ID}/v/{self.TEST_VIDEO_ID}',
            'long_description': '基本的指數',
            'related_exercises': [],
            'selected_nav_link': 'watch',
            'issue_labels': f'Component-Videos,Video-{self.TEST_VIDEO_ID}',
            'author_profile': 'https://plus.google.com/103970106103092409324',
            'content_rights_object': None,
            'access_control': '',
            'backup_timestamp': datetime(2020, 6, 16, 6, 6, 55, 146253, tzinfo=timezone.utc),
            'date_added': datetime(2012, 10, 11, 17, 48, 13, 565450, tzinfo=timezone.utc),
            'views': 491668,
            'subject_list': '[{"id": "basic-exponents", "title": '
                            '"\\u6307\\u6578(\\u57fa\\u790e)"}, {"id": "algebra", '
                            '"title": "\\u4ee3\\u6578"}]',
            'socrates_enabled': False,
        }.items(), video_data.items())

    @mock.patch('content.internal.socrates_question.SocratesQuestion.exists_for_video')
    def test_get_video_ok_with_socrates(self, socrates_patch):
        socrates_patch.return_value = True
        video_data = video_lib.get_video_play(topic_id=self.TEST_TOPIC_ID,
                                              video_id=self.TEST_VIDEO_ID)
        socrates_patch.assert_called_once_with(self.TEST_VIDEO_ID)
        self.assertTrue(video_data['socrates_enabled'])
