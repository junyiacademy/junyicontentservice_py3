# -*- coding: utf-8 -*-
import pickle
import unittest
import json
from unittest import mock

from content import exception
from . import internal
from . import mock_entity
from . import utils
from . import video as video_lib


class TestVideoProperty(unittest.TestCase):

    def setUp(self):
        self.mock_video_data = mock_entity.get_video_entity()

    def test_video_with_invalid_json_string_will_return_none(self):
        data = self.mock_video_data
        data['content_rights_object'] = 'The invalidate json string'
        video = video_lib.Video(entity=data)
        self.assertIsNone(
            video.content_rights_object
        )

    def test_video_without_content_rights_object(self):
        video = video_lib.Video(entity=self.mock_video_data)
        self.assertIsNone(
            video.content_rights_object
        )

    def test_video_with_content_rights_object(self):
        data = self.mock_video_data
        content_rights_object = {
            "rights_type": "CC",
            "content_link": "",
            "image_url": "https://licensebuttons.net/l/by-nc-nd/3.0/80x15.png",
            "author_name": "Code.org",
            "content_name": "How Computers Work: Binary & Data",
            "author_link": "https://code.org/",
            "image_link": "https://creativecommons.org/licenses/by-nc-nd/3.0/tw/"
        }
        data['content_rights_object'] = json.dumps(content_rights_object)
        video = video_lib.Video(entity=data)
        self.assertCountEqual(
            video.content_rights_object,
            content_rights_object
        )

    def test_video_titles_in_topic_is_dict(self):
        video = video_lib.Video(entity=self.mock_video_data)
        self.assertIsInstance(json.loads(video.titles_in_topic), dict)
    

class TestGetForReadableId(unittest.TestCase):

    def test_empty_readable_id(self):
        video = video_lib.get_for_readable_id('')
        self.assertIsNone(video)

    def test_non_exist_readable_id(self):
        video = video_lib.get_for_readable_id('non-exist-readable-id')
        self.assertIsNone(video)

    def test_existed_readable_id(self):
        video = video_lib.get_for_readable_id('the-why-of-the-3-divisibility-rule')
        self.assertIsNotNone(video)

    @mock.patch('content.internal.video._get_by_readable_id')
    def test_multiple_same_readable_id(self, get_by_readable_id_patch):
        obj1 = mock.MagicMock(**{'key.id': 100})
        obj1.__getitem__.side_effect = {'youtube_id': 'non_player_suffix'}.__getitem__
        obj2 = mock.MagicMock(**{'key.id': 200})
        obj2.__getitem__.side_effect = {'youtube_id': 'non_player_suffix'}.__getitem__
        obj3 = mock.MagicMock(**{'key.id': 300})
        obj3.__getitem__.side_effect = {'youtube_id': 'a_player'}.__getitem__
        get_by_readable_id_patch.return_value = [obj1, obj2, obj3]

        video = video_lib.get_for_readable_id('mock')
        self.assertEqual(video, obj2)


class TestVideoFromReadableId(unittest.TestCase):
    def setUp(self):
        self.addCleanup(mock.patch.stopall)
        self.version_mock = mock.patch('content.internal.internal.get_version').start()
        self.get_for_rid_mock = mock.patch('content.internal.video.get_for_readable_id').start()

        self._test_video_entity = mock_entity.get_video_entity()
        version_entity = mock_entity.get_topic_version_entity()
        self._test_content_change = \
            mock_entity.get_content_change_entity(version_entity.key,
                                                  self._test_video_entity.key,
                                                  ['title'])
        self.version_mock.return_value = version_entity
        
    def test_existed_readable_id(self):
        self.get_for_rid_mock.return_value = self._test_video_entity

        video = video_lib.Video.from_readable_id(mock.ANY, mock.ANY, is_applying_change=False)
        self.assertIsNotNone(video)
        self.assertIsInstance(video, video_lib.Video)

    def test_no_video_nor_change(self):
        self.get_for_rid_mock.return_value = None

        with self.assertRaises(exception.ContentNotExistsError):
            video = video_lib.Video.from_readable_id(mock.ANY, mock.ANY, is_applying_change=False)

    @mock.patch('content.internal.video.get_content_change')
    def test_existed_video_and_change(self, get_content_change_patch):
        self.get_for_rid_mock.return_value = self._test_video_entity
        get_content_change_patch.return_value = self._test_content_change

        video = video_lib.Video.from_readable_id(mock.ANY, mock.ANY, is_applying_change=True)
        changed_title = pickle.loads(self._test_content_change['content_changes'])['title']
        self.assertEqual(video.title, changed_title)
    
    @mock.patch('content.internal.internal.get_client')
    @mock.patch('content.internal.video.get_all_changes')
    def test_non_exist_video_but_change(self,
                                        get_all_changes_patch,
                                        get_client_patch):
        self.get_for_rid_mock.return_value = None
        get_all_changes_patch.return_value = [self._test_content_change]
        get_client_patch.return_value.get.return_value = self._test_video_entity
        readable_id = self._test_video_entity['readable_id']

        video = video_lib.Video.from_readable_id(readable_id, 
                                                 mock.ANY, 
                                                 is_applying_change=True, 
                                                 is_live_only=True)
        self.assertIsNotNone(video)
        self.assertEqual(video.readable_id, readable_id)
        changed_title = pickle.loads(self._test_content_change['content_changes'])['title']
        self.assertEqual(video.title, changed_title)

    @mock.patch('content.internal.internal.get_client')
    @mock.patch('content.internal.video.get_all_changes')
    def test_no_video_without_change_applied(self,
                                             get_all_changes_patch,
                                             get_client_patch):
        """
        Test the situation when no video is found within Video Entities,
        and no VersionContentChange referencing the Video.readable_id is found
        """
        self.get_for_rid_mock.return_value = None
        get_all_changes_patch.return_value = [self._test_content_change]
        get_client_patch.return_value.get.return_value = self._test_video_entity
        readable_id = f'{self._test_video_entity["readable_id"]}_no_matched'

        with self.assertRaises(exception.ContentNotExistsError):
            video = video_lib.Video.from_readable_id(readable_id, mock.ANY, is_applying_change=True)

        
class VideoTestSuite(unittest.TestCase):
    # FIXME(cowbon): Class name should not be named with 'suite'; it is inherited from unitest.TestCase
    # TODO(cowbon): Consider letting this TestCase be inherited by other testcases
    readable_id: str

    def tearDown(self):
        client = internal.get_client()
        entities = video_lib._get_by_readable_id(self.readable_id)
        for entity in entities:
            client.delete(entity.key)

    def check_entity_value(self, video_data):
        compared_keys = ['readable_id', 'title', 'description', 'youtube_id',
                         'duration', 'views', 'url', 'keywords']

        entities = []
        for i in range(5):
            entities = video_lib._get_by_readable_id(self.readable_id)
            if len(entities) > 0:
                break
            print('...retry %s' % (i + 1))
        self.assertEqual(len(entities), 1)
        video_entity = entities[0]

        version = internal.get_version('edit')
        existing_change = video_lib.get_content_change(video_entity, version)
        if existing_change:
            video_entity.update(pickle.loads(existing_change['content_changes']))

        for key in compared_keys:
            self.assertEqual(video_data[key], video_entity[key])


class TestCreateVideoEntity(VideoTestSuite):

    def test_create_video(self):
        video_data = mock_entity.get_video_entity()
        self.readable_id = video_data['readable_id']
        video_entity = video_lib.create_video(self.readable_id, video_data)

        self.assertIsNotNone(video_entity)
        self.check_entity_value(video_data)


class TestVideoDump(unittest.TestCase):

    def setUp(self) -> None:
        self._test_video_entity = mock_entity.get_video_entity()
        self._test_extended_slug = 'math/arithmetic/basic-exponents'
        self._mock_parent_topic = mock.MagicMock()
        self._mock_parent_topic.extended_slug = self._test_extended_slug

    def test_dump_video_data(self):
        test_video = video_lib.Video(entity=self._test_video_entity, parent_topic=self._mock_parent_topic)
        video_data = test_video.dump_video_data()
        self.assertEqual({
            'youtube_id': self._test_video_entity['youtube_id'],
            'url': self._test_video_entity['url'],
            'title': self._test_video_entity['title'],
            'titles_in_topic': self._test_video_entity['titles_in_topic'],
            'description': self._test_video_entity['description'],
            'keywords': self._test_video_entity['keywords'],
            'duration': self._test_video_entity['duration'],
            'grade': self._test_video_entity['grade'],
            'readable_id': self._test_video_entity['readable_id'],
            'canonical_url': f"/{self._test_extended_slug}/v/{self._test_video_entity['readable_id']}",
            'content_rights_object': None,
            'topic_string_keys': self._test_video_entity['topic_string_keys'],
            'views': self._test_video_entity['views'],
            'access_control': None,
            'subject_list': None,
            'date_added': self._test_video_entity['date_added'],
            'backup_timestamp': self._test_video_entity['backup_timestamp'],
            'is_hidden': None
        }, video_data)

    def test_dump_video_data_with_same_title_description(self):
        fake_tittle = 'same_fake_tittle'
        self._test_video_entity['description'] = fake_tittle
        self._test_video_entity['title'] = fake_tittle
        test_video = video_lib.Video(entity=self._test_video_entity, parent_topic=self._mock_parent_topic)
        video_data = test_video.dump_video_data()
        assert_data = {
            'youtube_id': self._test_video_entity['youtube_id'],
            'url': self._test_video_entity['url'],
            'title': fake_tittle,
            'titles_in_topic': self._test_video_entity['titles_in_topic'],
            'description': None,
            'keywords': self._test_video_entity['keywords'],
            'grade': self._test_video_entity['grade'],
            'duration': self._test_video_entity['duration'],
            'readable_id': self._test_video_entity['readable_id'],
            'canonical_url': f"/{self._test_extended_slug}/v/{self._test_video_entity['readable_id']}",
            'content_rights_object': None,
            'topic_string_keys': self._test_video_entity['topic_string_keys'],
            'views': self._test_video_entity['views'],
            'access_control': None,
            'subject_list': None,
            'date_added': self._test_video_entity['date_added'],
            'backup_timestamp': self._test_video_entity['backup_timestamp'],
            'is_hidden': None
        }
        self.assertEqual(assert_data, video_data)

    def test_dump_video_data_without_parent(self):
        test_video = video_lib.Video(entity=self._test_video_entity)
        with self.assertRaises(AttributeError):
            test_video.dump_video_data()

    def test_video_dot_dump(self):
        test_video = video_lib.Video(entity=self._test_video_entity, parent_topic=self._mock_parent_topic)
        assert_data = {
            'youtube_id': self._test_video_entity['youtube_id'],
            'url': self._test_video_entity['url'],
            'title': self._test_video_entity['title'],
            'titles_in_topic': self._test_video_entity['titles_in_topic'],
            'description': self._test_video_entity['description'],
            'keywords': self._test_video_entity['keywords'],
            'grade': self._test_video_entity['grade'],
            'duration': self._test_video_entity['duration'],
            'readable_id': self._test_video_entity['readable_id'],
            'topic_string_keys': self._test_video_entity['topic_string_keys'],
            'views': self._test_video_entity['views'],
            'subject_list': None,
            'access_control': None,
            'date_added': self._test_video_entity['date_added'],
            'backup_timestamp': self._test_video_entity['backup_timestamp'],
            'kind': 'Video',
            'relative_url': test_video.relative_url,
            'ka_url': test_video.ka_url,
            'id': self._test_video_entity['readable_id'],
            'is_live': True,
            'is_hidden': None
        }
        self.assertEqual(assert_data, test_video.dump())


class TestVideoSubtitles(unittest.TestCase):

    def setUp(self) -> None:
        self._test_video_sub_titles_entity = \
            mock_entity.get_video_subtitles_entity()

    def test_to_description(self):
        test_video_sub_titles = video_lib.VideoSubtitles(
            entity=self._test_video_sub_titles_entity)
        description = test_video_sub_titles.to_description()
        self.assertEqual('fake_json1 fake_json2', description)

    def test_to_description_with_long_text(self):
        long_text = '0123456789' * 10
        self._test_video_sub_titles_entity['json'] = \
            '[{"text": "' + long_text + '"}, {"text": "' + long_text + '"}]'
        test_video_sub_titles = video_lib.VideoSubtitles(
            entity=self._test_video_sub_titles_entity)
        description = test_video_sub_titles.to_description()
        self.assertEqual(' '.join((long_text, long_text))[:150], description)


class TestGetVideoSubtitles(unittest.TestCase):

    def test_empty_key(self):
        video_subtitles = video_lib._get_video_subtitles('')
        self.assertEqual(0, len(video_subtitles))

    def test_non_exist_key(self):
        video_subtitles = video_lib._get_video_subtitles('non-exist-key')
        self.assertEqual(0, len(video_subtitles))

    def test_exist_key(self):
        # There is no video subtitles in test data store
        video_subtitles = video_lib._get_video_subtitles('???')
        self.assertEqual(0, len(video_subtitles))


class TestGetVideoSubtitlesDescription(unittest.TestCase):

    @mock.patch.object(video_lib.VideoSubtitles, 'to_description',
                       return_value='fake_description')
    @mock.patch('content.internal.video._get_video_subtitles')
    def test_has_zh_tw_description(self, get_video_subtitles_patch, mock_to_description):
        get_video_subtitles_patch.return_value = mock_to_description

        description = video_lib._get_video_subtitles_description('fake_id')
        get_video_subtitles_patch.assert_called_once_with('zh-TW:fake_id')
        self.assertEqual('fake_description', description)

    @mock.patch.object(video_lib.VideoSubtitles, 'to_description',
                       return_value='fake_description')
    @mock.patch('content.internal.video._get_video_subtitles')
    def test_no_zh_tw_description(self, get_video_subtitles_patch, mock_to_description):
        get_video_subtitles_patch.side_effect = ([], mock_to_description)

        description = video_lib._get_video_subtitles_description('fake_id')
        get_video_subtitles_patch.assert_called_with('en:fake_id')
        self.assertEqual('fake_description', description)

    @mock.patch('content.internal.video._get_video_subtitles')
    def test_no_description(self, get_video_subtitles_patch):
        get_video_subtitles_patch.side_effect = ([], [])

        description = video_lib._get_video_subtitles_description('fake_id')
        self.assertIsNone(description)


class TestDumpNeighborVideoData(unittest.TestCase):

    def setUp(self) -> None:
        self._test_video_entity = mock_entity.get_video_entity()

    def test_dump_data(self):
        video_data = video_lib._dump_neighbor_video_data(self._test_video_entity)
        self.assertEqual({
            "key_id": self._test_video_entity.key.id,
            "readable_id": self._test_video_entity['readable_id'],
            "title": self._test_video_entity['title'],
        }, video_data)


class TestVideoFirstTopic(unittest.TestCase):

    def setUp(self):
        self.mock_topic_entity, _, _ = mock_entity.get_topic_entity()

    def test_none_topic_string_keys_will_return_none(self):
        entity = mock_entity.get_video_entity()
        video = video_lib.Video(entity=entity)
        self.assertIsNone(
            video.first_topic()
        )

    @mock.patch('google.cloud.datastore.key.Key')
    @mock.patch('content.internal.internal.get_client')
    def test_topic_string_key_invalid_will_return_none(
            self,
            mock_get_client,
            mock_key,
    ):
        def raise_exception():
            raise Exception()
        mock_key.from_legacy_urlsafe.side_effect = raise_exception
        entity = mock_entity.get_video_entity()
        entity['topic_string_keys'] = 'first\tsecond'
        video = video_lib.Video(entity=entity)
        topic = video.first_topic()
        self.assertIsNone(
            topic
        )
        mock_get_client.get.assert_not_called()

    @mock.patch('google.cloud.datastore.key.Key')
    @mock.patch('content.internal.internal.get_client')
    @mock.patch('content.internal.topic.Topic.get_parent')
    def test_no_topic_has_parent_will_return_none(
            self,
            mock_get_parent,
            mock_get_client,
            mock_key,
    ):
        mock_get_parent.return_value = None
        entity = mock_entity.get_video_entity()
        entity['topic_string_keys'] = 'first\tsecond'
        video = video_lib.Video(entity=entity)
        topic = video.first_topic()
        mock_key.from_legacy_urlsafe.assert_any_call(
            'first'
        )
        mock_key.from_legacy_urlsafe.assert_any_call(
            'second'
        )
        self.assertIsNone(
            topic
        )

    @mock.patch('google.cloud.datastore.key.Key')
    @mock.patch('content.internal.internal.get_client')
    @mock.patch('content.internal.topic.Topic.get_parent')
    def test_use_first_has_parent_topic(
            self,
            mock_get_parent,
            mock_get_client,
            mock_key,
    ):
        mock_get_client.return_value.get.return_value = self.mock_topic_entity
        mock_key.from_legacy_urlsafe.side_effect = str
        entity = mock_entity.get_video_entity()
        entity['topic_string_keys'] = 'first\tsecond'
        video = video_lib.Video(entity=entity)
        topic = video.first_topic()
        mock_key.from_legacy_urlsafe.assert_called_once_with('first')
        self.assertEqual(
            dict(topic._entity),
            dict(self.mock_topic_entity)
        )


class TestGetPlayData(unittest.TestCase):

    def setUp(self) -> None:
        self._test_video_entities_list = [
            mock_entity.get_video_entity(),
            mock_entity.get_video_entity(),
            mock_entity.get_video_entity(),
        ]
        self._mock_parent_topic = mock.MagicMock(**{
            'extended_slug': 'mock_slug'
        })

    def test_no_match(self):
        play_data, video = video_lib.get_play_data(
            self._mock_parent_topic, self._test_video_entities_list,
            'not_exist_readable_id')
        self.assertIsNone(play_data)
        self.assertIsNone(video)

    def _assert_video_and_play_data(self,
        matched_video_entity,
        play_data,
        video,
        previous_video_entity=None,
        next_video_entity=None):
        self.assertEqual(video.selected, 'selected')
        self.assertEqual(video.readable_id,
                         matched_video_entity['readable_id'])
        self.assertLessEqual({
            'previous_video': {
                'key_id': previous_video_entity.key.id,
                'readable_id': previous_video_entity['readable_id'],
                'title': previous_video_entity['title'],
            } if previous_video_entity else None,
            'next_video': {
                'key_id': next_video_entity.key.id,
                'readable_id': next_video_entity['readable_id'],
                'title': next_video_entity['title'],
            } if next_video_entity else None,
            'key':
                utils.get_legacy_key_str(matched_video_entity),
            'youtube_id':
                matched_video_entity['youtube_id'],
            'url':
                matched_video_entity['url'],
            'title':
                matched_video_entity['title'],
            'description':
                matched_video_entity['description'],
            'keywords':
                matched_video_entity['keywords'],
            'duration':
                matched_video_entity['duration'],
            'readable_id':
                matched_video_entity['readable_id'],
            'canonical_url':
                f"/mock_slug/v/{matched_video_entity['readable_id']}",
            'long_description': matched_video_entity['description'],
            'related_exercises': [],
            'selected_nav_link': 'watch',
            'issue_labels':
                f"Component-Videos,Video-{matched_video_entity['readable_id']}",
            'author_profile':
                'https://plus.google.com/103970106103092409324',
            'content_rights_object': None,
            'topic_string_keys': matched_video_entity['topic_string_keys'],
            'views': matched_video_entity['views'],
            'access_control': None,
            'subject_list': None,
            'date_added': matched_video_entity['date_added'],
            'backup_timestamp': matched_video_entity['backup_timestamp']
        }.items(), play_data.items())

    @mock.patch('content.internal.video.Video.first_topic')
    def test_match_first(self, mock_first_topic):
        matched_video_entity = self._test_video_entities_list[0]
        next_video_entity = self._test_video_entities_list[1]
        play_data, video = video_lib.get_play_data(
            self._mock_parent_topic, self._test_video_entities_list,
            matched_video_entity['readable_id'])

        self._assert_video_and_play_data(matched_video_entity, play_data, video,
                                         next_video_entity=next_video_entity)

    @mock.patch('content.internal.video.Video.first_topic')
    def test_match_middle(self, mock_first_topic):
        matched_video_entity = self._test_video_entities_list[1]
        prv_video_entity = self._test_video_entities_list[0]
        next_video_entity = self._test_video_entities_list[2]
        play_data, video = video_lib.get_play_data(
            self._mock_parent_topic, self._test_video_entities_list,
            matched_video_entity['readable_id'])

        self._assert_video_and_play_data(matched_video_entity, play_data, video,
                                         previous_video_entity=prv_video_entity,
                                         next_video_entity=next_video_entity)

    @mock.patch('content.internal.video.Video.first_topic')
    def test_match_last(self, first_topic):
        matched_video_entity = self._test_video_entities_list[2]
        prv_video_entity = self._test_video_entities_list[1]
        play_data, video = video_lib.get_play_data(
            self._mock_parent_topic, self._test_video_entities_list,
            matched_video_entity['readable_id'])

        self._assert_video_and_play_data(matched_video_entity, play_data, video,
                                         previous_video_entity=prv_video_entity)

    def test_the_video_first_topic_is_none_will_raise_missing_video_error(self):
        matched_video_entity = self._test_video_entities_list[2]
        with self.assertRaises(exception.MissingVideoError):
            video_lib.get_play_data(
                self._mock_parent_topic, self._test_video_entities_list,
                matched_video_entity['readable_id']
            )

    @mock.patch('content.internal.video.Video.first_topic')
    @mock.patch('content.internal.video._get_video_subtitles_description')
    def test_no_subtitles_description_will_use_description_as_long_description(
            self,
            mock_get_video_subtitles_description,
            mock_first_topic,
    ):
        mock_get_video_subtitles_description.return_value = None
        matched_video_entity = self._test_video_entities_list[2]
        play_data, _ = video_lib.get_play_data(
            self._mock_parent_topic,
            self._test_video_entities_list,
            matched_video_entity['readable_id']
        )
        self.assertIn(
            'long_description',
            play_data
        )
        self.assertEqual(
            play_data['long_description'],
            play_data['description']
        )
        self.assertIsNotNone(
            play_data['long_description']
        )
