# -*- coding: utf-8 -*-
import copy
import datetime
import json
import pickle
import re
import urllib.parse
from typing import Optional

from google.cloud import datastore

import instance_cache
from auth import user_util
from content import exception
from content import helpers
from content.internal import internal, topic
from content.internal.content_prototype import Content
from content import factory


EXAM_ROOT_TOPIC_ID = 'junyiacademy-exam'
EXCLUDE_FROM_INDEXES = (
    'description',
)


class Exercise(Content):
    name: str
    pretty_display_name: str
    live: bool
    description: str
    is_quiz_exercise: bool
    large_screen_only: bool
    is_hidden: bool
    topic_string_keys: str

    _property_from_entity = [
        'name',
        'pretty_display_name',
        'live',
        'description',
        'is_quiz_exercise',
        'large_screen_only',
        'is_hidden',
        'topic_string_keys',
    ]

    @property
    def is_live(self):
        return self.live

    @property
    def is_teaching_material(self):
        return True

    @property
    def presented_title(self):
        pretty_display_name = self.pretty_display_name
        if not pretty_display_name:
            import logging
            logging.error("exercise has no pretty_display_name. name: %s", self.name)
            # fallback to use its name (id)
            pretty_display_name = self.name
        if self.is_live:
            return pretty_display_name
        return pretty_display_name + ' [hidden]'

    @property
    def progress_id(self):
        return self.name

    @property
    def canonical_url(self):
        if not self._parent_topic:
            raise AttributeError('Require parent topic to get url')
        return "/%s/e/%s" % (self._parent_topic.extended_slug, urllib.parse.quote(self.name))

    def __init__(self, entity=None, parent_topic=None):
        super().__init__(entity, parent_topic)

    def info_to_topic_page(self):
        return {
            'url': self.canonical_url,
            'type': self.__class__.__name__,
            'title': self.presented_title,
            'description': self.description,
            'progress_id': self.progress_id,
            'id': self.name,
            'is_content': self.is_teaching_material,
            'is_quiz_exercise': self.is_quiz_exercise,
            'large_screen_only': self.large_screen_only
        }

    def dump(self, props: Optional[list] = None) -> dict:
        return {
            'kind': self.__class__.__name__,
            'id': self.name,
            'title': self.pretty_display_name or '',
            'is_live': self.is_live,
        }

    @staticmethod
    def get_all_use_cache(live_only=False, include_exam=True):
        if user_util.User.current() and user_util.User.current().is_developer and not live_only:
            exercises = Exercise._get_all_use_cache_unsafe(include_exam=include_exam)
        else:
            exercises = Exercise._get_all_use_cache_safe()
        return list(map(
            Exercise,
            exercises
        ))

    @staticmethod
    def _get_all_use_cache_safe():
        return filter(
            lambda exercise: (
                exercise.get('live', None) and
                exercise.get('topic_string_keys', [])
            ),
            Exercise._get_all_use_cache_unsafe(include_exam=False)
        )

    @staticmethod
    @instance_cache.cache_with_key_fxn(
        lambda * arg, **kwargs: "all_exercises_unsafe_include_exam_%s_%s" %
        (kwargs['include_exam'] if 'include_exam' in kwargs else 'True', internal.get_setting("cached_exercises_date")))
    def _get_all_use_cache_unsafe(include_exam=True):
        query = Exercise._all_unsafe()
        all_exercises = query.fetch(None)
        # [TODO] Figure out another way or function to remove exam exercises
        # because _get_all_use_cache_unsafe should return all exercises.
        # EN says he would figure out. -by Benny
        if not include_exam:
            all_exercises = Exercise._remove_exam_exercises(all_exercises)
        return all_exercises

    @classmethod
    def _all_unsafe(cls):
        datastore_client = internal.get_client()
        query = datastore_client.query(kind='Exercise')
        return query

    @staticmethod
    def _remove_exam_exercises(all_exercises):
        exam_exercise_name_list = Exercise.get_all_exam_exercise_names()
        return filter(
            lambda exercise:
            exercise.get('name', None) not in exam_exercise_name_list,
            all_exercises
        )

    @staticmethod
    @instance_cache.cache_with_key_fxn(
        lambda * arg, **kwargs: "all_exam_exercise_names_%s" %
        internal.get_setting("cached_exercises_date")
    )
    def get_all_exam_exercise_names():
        exercise_names_list = []
        exam_topic_root = topic.Topic.from_id(topic_id=EXAM_ROOT_TOPIC_ID)
        if exam_topic_root:
            exam_topic_root.content_factory = factory.get_content_factory()
            exam_exercises = exam_topic_root.get_exercises(
                include_descendants=True,
                include_hidden=True
            )
            exercise_names_list = [exercise['name'] for exercise in exam_exercises]
        return exercise_names_list


def get_by_name(exercise_name):
    client = internal.get_client()
    query = client.query(kind='Exercise')
    query.add_filter('name', '=', exercise_name)
    return next(iter(query.fetch(1)), None)


def add_new_exercise(data):
    client = internal.get_client()
    exercise = datastore.Entity(key=client.key('Exercise'), exclude_from_indexes=EXCLUDE_FROM_INDEXES)
    exercise.update(data)
    exercise['backup_timestamp'] = datetime.datetime.utcnow()
    exercise['creation_date'] = datetime.datetime.utcnow()
    exercise['is_hidden'] = True
    client.put(exercise)
    return exercise


# TODO(kamesan): move to a common place to share with video api impl
def get_content_change(content, version):
    query = internal.get_query('VersionContentChange', ancestor=version.key)
    query.add_filter('version', '=', version.key)
    query.add_filter('content', '=', content.key)
    return next(iter(query.fetch(1)), None)


# TODO(kamesan): move to a common place to share with video api impl
def add_content_change(content, version, data):
    client = internal.get_client()

    change = get_content_change(content, version)
    diff = pickle.loads(change['content_changes']) if change is not None else {}
    orig_data = copy.copy(dict(content))
    orig_data.update(diff)

    updated = False
    for prop, value in data.items():
        if value is None:
            continue
        if prop in orig_data and orig_data[prop] == value:
            continue
        diff[prop] = value
        updated = True

    # only put the change if we have actually changed any props
    if updated:
        if change is None:
            change = datastore.Entity(
                key=client.key('VersionContentChange', parent=version.key),
                exclude_from_indexes=('content_changes',)
            )
            change['version'] = version.key
            change['content'] = content.key
        change['content_changes'] = pickle.dumps(diff, protocol=2)
        change['updated_on'] = datetime.datetime.utcnow()
        change['last_edited_by'] = None  # FIXME: current user
        client.put(change)


def validate_name_format(exercise_name):
    if not exercise_name:
        raise exception.InvalidFormat(exercise_name, "代號不能空白")
    if len(exercise_name) > 500:
        raise exception.InvalidFormat(exercise_name, "代號不能超過 500 個字, %s" % exercise_name)
    if not re.match(r'^[a-z0-9_-]+$', exercise_name):
        raise exception.InvalidFormat(exercise_name, "代號 %s 存在不合法字元" % exercise_name)


def validate_title_format(content_title):
    if not content_title:
        raise exception.InvalidFormat(content_title, "標題不能空白")
    if len(content_title) > 100:
        raise exception.InvalidFormat(content_title, "標題不能超過 100 個字, %s" % content_title)


def validate_consistency(exercise_name, content_title, description, parent_id, grade):
    new_exercise_data = _make_new_exercise_data(content_title, description, exercise_name, grade)

    version = internal.get_edit_version()
    root_key = topic.get_root_key(version)
    parent_topic = _get_parent_topic(parent_id, root_key, version)

    existing_exercise = get_by_name(exercise_name)
    if existing_exercise:
        _validate_consistency(existing_exercise, new_exercise_data, parent_topic, version)


def create_exercise(exercise_name, content_title, description, parent_id, grade):
    new_exercise_data = _make_new_exercise_data(content_title, description, exercise_name, grade)

    # Get the root entities here since only ancestor queries are allowed inside
    # transactions.
    client = internal.get_client()
    version = internal.get_edit_version()
    root_key = topic.get_root_key(version)
    existing_exercise = get_by_name(exercise_name)
    if not existing_exercise:
        new_exercise_data['is_hidden'] = True

    parent_topic = _get_parent_topic(parent_id, root_key, version)
    # 更新 topic_string_keys
    new_exercise_data['topic_string_keys'] = helpers.get_updated_topic_string_keys(parent_topic, existing_exercise)

    with client.transaction():
        exercise, parent_topic = _create_exercise_txn(
                parent_id, version, root_key, existing_exercise, new_exercise_data)

    if existing_exercise is None:
        # Add exercise to parent topic here since the key is incomplete inside
        # the transaction.
        if 'child_keys' not in parent_topic:
            parent_topic['child_keys'] = []
        parent_topic['child_keys'].append(exercise.key)
        client.put(parent_topic)


def update_exercise(exercise_name, content_title, description, grade):
    new_exercise_data = {
        'description': description,
        'pretty_display_name': content_title,
        'short_display_name': content_title[:11],
        'grade': grade,
    }

    exercise = get_by_name(exercise_name)
    if exercise is None:
        raise exception.ExerciseUpdateError("這個代號的知識點不存在")

    client = internal.get_client()
    version = internal.get_edit_version()

    with client.transaction():
        add_content_change(exercise, version, new_exercise_data)


def _create_exercise_txn(parent_id, version, root_key, existing_exercise, new_exercise_data):
    parent_topic = _get_parent_topic(parent_id, root_key, version)

    if existing_exercise is None:
        exercise = add_new_exercise(new_exercise_data)
        # Adding the exercise to parent topic needs to defer until the
        # transaction is committed to obtain a complete key.
        return exercise, parent_topic

    _validate_consistency(existing_exercise, new_exercise_data, parent_topic, version)

    add_content_change(existing_exercise, version, new_exercise_data)

    # Since the exercise exists and has a complete key, we can add it to the
    # parent topic here.
    client = internal.get_client()
    if 'child_keys' not in parent_topic:
        parent_topic['child_keys'] = []
    parent_topic['child_keys'].append(existing_exercise.key)
    client.put(parent_topic)

    return existing_exercise, parent_topic


def _make_new_exercise_data(content_title, description, exercise_name, grade):
    return {
        'cover_range': json.dumps([[exercise_name]]),
        'covers': [],
        'description': description,
        'grade': grade,
        'h_position': 0,
        'is_quiz_exercise': True,
        'live': True,
        'name': exercise_name,
        'prerequisites': [],
        'pretty_display_name': content_title,
        'seconds_per_fast_problem': 0.0,
        'short_display_name': content_title[:11],
        'tags': [],
        'v_position': 0,
    }


def _get_parent_topic(parent_id, root_key, version):
    # TODO(kamesan): rename _get_by_id to get_by_id
    parent_topic = topic._get_by_id(parent_id, version, ancestor=root_key)
    if parent_topic is None:
        raise exception.ExerciseCreateError("預計擺放的母資料夾 [%s] 不存在" % parent_id)
    return parent_topic


def _validate_consistency(existing_exercise, new_exercise_data, parent_topic, version):
    # Validate the new data is consistent with the existing exercise.
    exercise = copy.copy(existing_exercise)
    existing_change = get_content_change(existing_exercise, version)
    if existing_change is not None:
        exercise.update(pickle.loads(existing_change['content_changes']))
    if 'child_keys' in parent_topic and exercise.key in parent_topic['child_keys']:
        raise exception.ExerciseCreateError(
            "母資料夾中，已存在相同標題的知識點 [%s]" % exercise['pretty_display_name'])
    if new_exercise_data['pretty_display_name'] != exercise['pretty_display_name']:
        raise exception.ExerciseCreateError(
            "此知識點已存在，且已存在的知識點標題 [%s] 和預計上架的標題不一樣" % exercise['pretty_display_name'])
    if new_exercise_data['cover_range'] != exercise['cover_range']:
        raise exception.ExerciseCreateError(
            "此知識點已存在，且已存在的知識點 cover range [%s] 和預計上架的 cover range 不一樣" % exercise['cover_range'])
    if new_exercise_data['grade'] != exercise['grade']:
        raise exception.ExerciseCreateError("此知識點已存在，且已存在的知識點的年級和預計上架的年級不一樣")
