# -*- coding: utf-8 -*-
from typing import Optional

from .content_prototype import Content


class Iframe(Content):
    id: str
    title: str

    _property_from_entity = ['id', 'title']

    @property
    def is_live(self):
        return True

    @property
    def is_teaching_material(self):
        return True

    @property
    def presented_title(self):
        if self.is_live:
            return self.title
        return self.title + ' [hidden]'

    @property
    def url(self):
        if not self._parent_topic:
            raise AttributeError('Require parent topic to get url')
        return '/%s/i/%s' % (self._parent_topic.id, self.id)

    def __init__(self, entity=None, parent_topic=None):
        super().__init__(entity=entity)
        if parent_topic:
            self._parent_topic = parent_topic

    def info_to_topic_page(self):
        return {
            'type': self.__class__.__name__,
            'id': self.id,
            'url': self.url,
            'title': self.presented_title,
            'description': '',
            'is_content': self.is_teaching_material,
        }

    def dump(self, props: Optional[list] = None) -> dict:
        return {
            'kind': self.__class__.__name__,
            'id': self.id,
            'title': self.title,
            'is_live': self.is_live,
        }
