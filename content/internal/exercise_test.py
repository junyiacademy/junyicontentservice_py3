# -*- coding: utf-8 -*-
import copy
import datetime
import json
import pickle
import random
import re
import unittest
from itertools import product
from unittest.mock import patch, Mock

from content import exception
from content import helpers
from content.internal import exercise
from content.internal import mock_entity



def mock_cache_side_effect(
        target,
        key_fxn,
        available_seconds,
        category,
        *args,
        **kwargs
):
    return target(*args, **kwargs)


class TestExercise(unittest.TestCase):
    @patch('google.cloud.datastore.Client.put')
    @patch('content.internal.exercise.get_content_change')
    @patch('content.internal.exercise.get_by_name')
    @patch('content.internal.topic._get_by_id')
    @patch('content.internal.topic.get_root_key')
    @patch('content.internal.internal.get_edit_version')
    def test_create_exercise(self, get_version_patch, _, get_topic_patch, get_exercise_patch, get_change_patch, put_patch):
        version = mock_entity.get_topic_version_entity()
        content = mock_entity.get_exercise_entity()
        change = mock_entity.get_content_change_entity(version.key, content.key, ['description'])
        orig_desc = pickle.loads(change['content_changes'])['description']
        parent_topic, _, _ = mock_entity.get_topic_entity()
        get_version_patch.return_value = version

        # Test parent topic not exist.
        get_topic_patch.return_value = None
        get_exercise_patch.return_value = None
        self.assertRaisesRegex(
                exception.ExerciseCreateError,
                r"^預計擺放的母資料夾 \[%s\] 不存在$" % re.escape(parent_topic['id']),
                exercise.create_exercise,
                content['name'], content['pretty_display_name'], content['description'],
                parent_topic['id'],
                content['grade'])
        put_patch.assert_not_called()

        # Test exercise exists in the parent topic.
        parent_topic['child_keys'].append(content.key)
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = None
        self.assertRaisesRegex(
                exception.ExerciseCreateError,
                r"^母資料夾中，已存在相同標題的知識點 \[%s\]$" % re.escape(content['pretty_display_name']),
                exercise.create_exercise,
                content['name'], content['pretty_display_name'], content['description'],
                parent_topic['id'],
                content['grade'])
        put_patch.assert_not_called()
        parent_topic['child_keys'].pop()

        # Test exercise exists with different title.
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = copy.deepcopy(content)
        self.assertRaisesRegex(
                exception.ExerciseCreateError,
                r"^此知識點已存在，且已存在的知識點標題 \[%s\] 和預計上架的標題不一樣$" % re.escape(content['pretty_display_name']),
                exercise.create_exercise,
                content['name'], content['pretty_display_name'] + 'x', content['description'],
                parent_topic['id'],
                content['grade'])
        put_patch.assert_not_called()

        # Test exercise exists with different cover range.
        orig_cover_range = json.loads(content['cover_range'])
        content['cover_range'] = json.dumps(orig_cover_range + [['test_ex_name']])
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = copy.deepcopy(content)
        self.assertRaisesRegex(
                exception.ExerciseCreateError,
                r"^此知識點已存在，且已存在的知識點 cover range \[%s\] 和預計上架的 cover range 不一樣$" % re.escape(str(content['cover_range'])),
                exercise.create_exercise,
                content['name'], content['pretty_display_name'], content['description'],
                parent_topic['id'],
                content['grade'])
        put_patch.assert_not_called()
        content['cover_range'] = json.dumps(orig_cover_range)

        # Test exercise not exist.
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = None
        exercise.create_exercise(
                content['name'], content['pretty_display_name'], content['description'],
                parent_topic['id'],
                content['grade'])
        # Should put a new Exercise and a changed Topic.
        self.assertEqual(put_patch.call_count, 2)
        entities = [e for ((e,), _) in put_patch.call_args_list]
        self.assertEqual({e.key.kind for e in entities}, {'Exercise', 'Topic'})
        for entity in entities:
            if entity.key.kind == 'Exercise':
                self.assertEqual(entity.key.flat_path, ('Exercise',))
                required_props = {
                        'cover_range': json.dumps([[content['name']]]),
                        'covers': [],
                        'description': content['description'],
                        'h_position': 0,
                        'is_quiz_exercise': True,
                        'live': True,
                        'name': content['name'],
                        'prerequisites': [],
                        'pretty_display_name': content['pretty_display_name'],
                        'seconds_per_fast_problem': 0.0,
                        'short_display_name': content['pretty_display_name'][:11],
                        'tags': [],
                        'v_position': 0,
                }
                self.assertEqual({k: entity[k] for k in required_props}, required_props)
            elif entity.key.kind == 'Topic':
                exercise_key = entity['child_keys'].pop()
                self.assertEqual(entity, parent_topic)
                # The appended key is not complete since Client.put is mocked.
                self.assertEqual(exercise_key.flat_path, ('Exercise',))
        put_patch.reset_mock()

        # Test exercise not exist and parent topic doesn't have 'child_keys'
        # property.
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        del get_topic_patch.return_value['child_keys']
        get_exercise_patch.return_value = None
        exercise.create_exercise(
                content['name'], content['pretty_display_name'], content['description'],
                parent_topic['id'],
                content['grade'])
        # Should put a new Exercise and a changed Topic.
        self.assertEqual(put_patch.call_count, 2)
        entities = [e for ((e,), _) in put_patch.call_args_list]
        self.assertEqual({e.key.kind for e in entities}, {'Exercise', 'Topic'})
        for entity in entities:
            if entity.key.kind == 'Exercise':
                self.assertEqual(entity.key.flat_path, ('Exercise',))
                required_props = {
                        'cover_range': json.dumps([[content['name']]]),
                        'covers': [],
                        'description': content['description'],
                        'h_position': 0,
                        'is_quiz_exercise': True,
                        'live': True,
                        'name': content['name'],
                        'prerequisites': [],
                        'pretty_display_name': content['pretty_display_name'],
                        'seconds_per_fast_problem': 0.0,
                        'short_display_name': content['pretty_display_name'][:11],
                        'tags': [],
                        'v_position': 0,
                }
                self.assertEqual({k: entity[k] for k in required_props}, required_props)
            elif entity.key.kind == 'Topic':
                exercise_key = entity['child_keys'].pop()
                self.assertEqual(entity, parent_topic)
                # The appended key is not complete since Client.put is mocked.
                self.assertEqual(exercise_key.flat_path, ('Exercise',))
        put_patch.reset_mock()

        # Test exercise exists.
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = None
        exercise.create_exercise(
                content['name'], content['pretty_display_name'], content['description'],
                parent_topic['id'],
                content['grade'])
        # Should put a changed Topic.
        put_patch.assert_called()
        (entity,) = put_patch.call_args[0]
        parent_topic['child_keys'].append(content.key)
        self.assertEqual(entity, parent_topic)
        parent_topic['child_keys'].pop()
        put_patch.reset_mock()

        # Test exercise exists and parent topic doesn't have 'child_keys'
        # property.
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        del get_topic_patch.return_value['child_keys']
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = None
        exercise.create_exercise(
                content['name'], content['pretty_display_name'], content['description'],
                parent_topic['id'],
                content['grade'])
        # Should put a changed Topic.
        put_patch.assert_called()
        (entity,) = put_patch.call_args[0]
        parent_topic['child_keys'] = [content.key]
        self.assertEqual(entity, parent_topic)
        parent_topic['child_keys'] = []
        put_patch.reset_mock()

        # Test exercise/change exists.
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = copy.deepcopy(change)
        exercise.create_exercise(
                content['name'], content['pretty_display_name'], orig_desc,
                parent_topic['id'],
                content['grade'])
        # Should put a changed Topic.
        put_patch.assert_called()
        (entity,) = put_patch.call_args[0]
        parent_topic['child_keys'].append(content.key)
        self.assertEqual(entity, parent_topic)
        parent_topic['child_keys'].pop()
        put_patch.reset_mock()

        # Test exercise exists and put a change.
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = None
        new_desc = orig_desc + ' (new)'
        exercise.create_exercise(
                content['name'], content['pretty_display_name'], new_desc,
                parent_topic['id'],
                content['grade'])
        # Should put a new VersionContentChange and a changed Topic.
        entities = [e for ((e,), _) in put_patch.call_args_list]
        self.assertEqual({e.key.kind for e in entities}, {'VersionContentChange', 'Topic'})
        for entity in entities:
            if entity.key.kind == 'VersionContentChange':
                self.assertEqual(entity.key.flat_path, version.key.flat_path + ('VersionContentChange',))
                self.assertEqual(entity['version'], version.key)
                self.assertEqual(entity['content'], content.key)
                self.assertEqual(entity['last_edited_by'], None)
                self.assertEqual(type(entity['updated_on']), datetime.datetime)
            elif entity.key.kind == 'Topic':
                parent_topic['child_keys'].append(content.key)
                self.assertEqual(entity, parent_topic)
                parent_topic['child_keys'].pop()
        put_patch.reset_mock()

        # Test exercise/change exists and put a change.
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = copy.deepcopy(change)
        new_desc = orig_desc + ' (new)'
        exercise.create_exercise(
                content['name'], content['pretty_display_name'], new_desc,
                parent_topic['id'],
                content['grade'])
        # Should put a changed VersionContentChange and a changed Topic.
        entities = [e for ((e,), _) in put_patch.call_args_list]
        self.assertEqual({e.key.kind for e in entities}, {'VersionContentChange', 'Topic'})
        for entity in entities:
            if entity.key.kind == 'VersionContentChange':
                self.assertEqual(entity.key, change.key)
                self.assertEqual(entity['version'], version.key)
                self.assertEqual(entity['content'], content.key)
                self.assertEqual(entity['last_edited_by'], None)
                self.assertEqual(type(entity['updated_on']), datetime.datetime)
            elif entity.key.kind == 'Topic':
                parent_topic['child_keys'].append(content.key)
                self.assertEqual(entity, parent_topic)
                parent_topic['child_keys'].pop()
        put_patch.reset_mock()

    @patch('google.cloud.datastore.Client.put')
    @patch('content.internal.exercise.get_content_change')
    @patch('content.internal.exercise.get_by_name')
    @patch('content.internal.topic._get_by_id')
    @patch('content.internal.topic.get_root_key')
    @patch('content.internal.internal.get_edit_version')
    def test_validate_consistency(self, get_version_patch, _, get_topic_patch, get_exercise_patch, get_change_patch, put_patch):
        version = mock_entity.get_topic_version_entity()
        content = mock_entity.get_exercise_entity()
        parent_topic, _, _ = mock_entity.get_topic_entity()
        get_version_patch.return_value = version

        # Test parent topic not exist.
        get_topic_patch.return_value = None
        get_exercise_patch.return_value = None
        self.assertRaisesRegex(
            exception.ExerciseCreateError,
            r"^預計擺放的母資料夾 \[%s\] 不存在$" % re.escape(parent_topic['id']),
            exercise.validate_consistency,
            content['name'], content['pretty_display_name'], content['description'],
            parent_topic['id'],
            content['grade'])
        put_patch.assert_not_called()

        # Test exercise exists in the parent topic.
        parent_topic['child_keys'].append(content.key)
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = None
        self.assertRaisesRegex(
            exception.ExerciseCreateError,
            r"^母資料夾中，已存在相同標題的知識點 \[%s\]$" % re.escape(content['pretty_display_name']),
            exercise.validate_consistency,
            content['name'], content['pretty_display_name'], content['description'],
            parent_topic['id'],
            content['grade'])
        put_patch.assert_not_called()
        parent_topic['child_keys'].pop()

        # Test exercise exists with different title.
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = copy.deepcopy(content)
        self.assertRaisesRegex(
            exception.ExerciseCreateError,
            r"^此知識點已存在，且已存在的知識點標題 \[%s\] 和預計上架的標題不一樣$" % re.escape(content['pretty_display_name']),
            exercise.validate_consistency,
            content['name'], content['pretty_display_name'] + 'x', content['description'],
            parent_topic['id'],
            content['grade'])
        put_patch.assert_not_called()

        # Test exercise exists with different grade.
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = copy.deepcopy(content)
        different_grade = 1
        self.assertRaisesRegex(
            exception.ExerciseCreateError,
            r"^此知識點已存在，且已存在的知識點的年級和預計上架的年級不一樣$",
            exercise.validate_consistency,
            content['name'], content['pretty_display_name'], content['description'],
            parent_topic['id'],
            different_grade)
        put_patch.assert_not_called()

        # Test exercise exists with different cover range.
        orig_cover_range = json.loads(content['cover_range'])
        content['cover_range'] = json.dumps(orig_cover_range + [['test_ex_name']])
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = copy.deepcopy(content)
        self.assertRaisesRegex(
            exception.ExerciseCreateError,
            r"^此知識點已存在，且已存在的知識點 cover range \[%s\] 和預計上架的 cover range 不一樣$" % re.escape(str(content['cover_range'])),
            exercise.validate_consistency,
            content['name'], content['pretty_display_name'], content['description'],
            parent_topic['id'],
            content['grade'])
        put_patch.assert_not_called()
        content['cover_range'] = json.dumps(orig_cover_range)

    @patch('google.cloud.datastore.Client.put')
    @patch('content.internal.exercise.get_content_change')
    @patch('content.internal.exercise.get_by_name')
    @patch('content.internal.internal.get_edit_version')
    def test_update_exercise(self, get_version_patch, get_exercise_patch, get_change_patch, put_patch):
        version = mock_entity.get_topic_version_entity()
        content = mock_entity.get_exercise_entity()
        change = mock_entity.get_content_change_entity(version.key, content.key, ['description'])
        orig_desc = pickle.loads(change['content_changes'])['description']
        get_version_patch.return_value = version

        # Test exercise not found.
        get_exercise_patch.return_value = None
        self.assertRaisesRegex(
                exception.ExerciseUpdateError, r"^這個代號的知識點不存在$",
                exercise.update_exercise, content['name'], 'test_title', 'test_desc', content['grade'])
        put_patch.assert_not_called()

        # Test dummy update with existing exercise and no change.
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = None
        exercise.update_exercise(
                content['name'], content['pretty_display_name'], content['description'], content['grade'])
        put_patch.assert_not_called()

        # Test dummy update with existing exercise and existing change.
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = copy.deepcopy(change)
        exercise.update_exercise(
                content['name'], content['pretty_display_name'], orig_desc, content['grade'])
        put_patch.assert_not_called()

        # Test update with existing exercise and no change.
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = None
        exercise.update_exercise(
                content['name'], content['pretty_display_name'], orig_desc, content['grade'])
        put_patch.assert_called_once()
        (entity,) = put_patch.call_args[0]
        self.assertEqual(entity.key.flat_path, version.key.flat_path + ('VersionContentChange',))
        self.assertEqual(entity['version'], version.key)
        self.assertEqual(entity['content'], content.key)
        self.assertEqual(entity['content_changes'], pickle.dumps({'description': orig_desc}, protocol=2))
        self.assertEqual(entity['last_edited_by'], None)
        self.assertEqual(type(entity['updated_on']), datetime.datetime)
        put_patch.reset_mock()

        # Test update with existing exercise and existing change.
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = copy.deepcopy(change)
        new_desc = orig_desc + ' (new)'
        exercise.update_exercise(
                content['name'], content['pretty_display_name'], new_desc, content['grade'])
        put_patch.assert_called_once()
        (entity,) = put_patch.call_args[0]
        self.assertEqual(entity.key, change.key)
        self.assertEqual(entity['version'], version.key)
        self.assertEqual(entity['content'], content.key)
        self.assertEqual(entity['content_changes'], pickle.dumps({'description': new_desc}, protocol=2))
        self.assertEqual(entity['last_edited_by'], None)
        self.assertEqual(type(entity['updated_on']), datetime.datetime)

    @patch('instance_cache.request_cache_check_set_return')
    @patch('content.internal.exercise.topic.Topic.from_id')
    def test_get_all_exam_exercise_names_on_topic_not_found(
            self, mock_topic_from_id, mock_cache
    ):
        mock_cache.side_effect = mock_cache_side_effect

        mock_topic_from_id.return_value = None
        exercise_names = exercise.Exercise.get_all_exam_exercise_names()
        self.assertEqual(
            exercise_names,
            []
        )

    @patch('instance_cache.request_cache_check_set_return')
    @patch('content.internal.exercise.topic.Topic.from_id')
    def test_get_all_exam_exercise_names(self, mock_topic_from_id, mock_cache):
        mock_cache.side_effect = mock_cache_side_effect
        mock_topic = Mock()
        names = [f'name_{num}' for num in range(20)]
        mock_topic.get_exercises.return_value = [
            dict(name=name)
            for name in names
        ]
        mock_topic_from_id.return_value = mock_topic
        exercise_names = exercise.Exercise.get_all_exam_exercise_names()
        self.assertEqual(
            names,
            exercise_names
        )

    @patch('content.internal.exercise.Exercise.get_all_exam_exercise_names')
    def test_exercise_remove_exam_exercises(
            self, mock_get_all_exam_exercise_names
    ):
        exam_exercise_names = ['exam']
        mock_get_all_exam_exercise_names.return_value = exam_exercise_names
        exercises = [dict(name='exam'), dict(name='normal')]
        exercises = list(exercise.Exercise._remove_exam_exercises(exercises))
        self.assertEqual(
            len(exercises),
            1
        )
        for exercise_ in exercises:
            self.assertNotIn(
                exercise_['name'],
                exam_exercise_names
            )

    @patch('content.internal.exercise.internal.get_client')
    def test_exercise_all_unsafe(self, mock_get_client):
        mock_client_return = Mock()
        mock_query = Mock()
        mock_get_client.side_effect = lambda *args, **kargs: mock_client_return
        mock_client_return.query.side_effect = lambda *args, **kargs: mock_query
        all_ = exercise.Exercise._all_unsafe()
        mock_client_return.query.assert_called_with(kind='Exercise')
        self.assertIs(all_, mock_query)

    @patch('instance_cache.request_cache_check_set_return')
    @patch('content.internal.exercise.Exercise._all_unsafe')
    def test_get_all_use_cache_unsafe_include_exam(self, mock_all, mock_cache):
        mock_cache.side_effect = mock_cache_side_effect
        query = Mock()
        mock_all.side_effect = lambda *args, **kwargs: query

        all_ = exercise.Exercise._get_all_use_cache_unsafe()

        mock_cache.assert_called_once()
        self.assertEqual(
            all_,
            query.fetch()
        )

    @patch('instance_cache.request_cache_check_set_return')
    @patch('content.internal.exercise.Exercise._remove_exam_exercises')
    @patch('content.internal.exercise.Exercise._all_unsafe')
    def test_get_all_use_cache_unsafe_exclude_exam(
            self, mock_all, mock_remove_exam_exercises, mock_cache
    ):
        mock_cache.side_effect = mock_cache_side_effect
        mock_remove_exam_exercises.side_effect = lambda data: data
        query = Mock()
        mock_all.side_effect = lambda *args, **kwargs: query

        all_ = exercise.Exercise._get_all_use_cache_unsafe(
            False
        )

        mock_cache.assert_called_once()
        mock_remove_exam_exercises.assert_called_once()
        self.assertEqual(
            all_,
            query.fetch()
        )

    @patch('content.internal.exercise.Exercise._get_all_use_cache_unsafe')
    def test_get_all_use_cache_safe(self, mock_get_all_use_cache_unsafe):
        class Exercise(dict):
            pass
        all_unsafe = [
            Exercise(),
            Exercise(live=False),
            Exercise(topic_string_keys=[]),
            Exercise(live=True, topic_string_keys=[]),
            Exercise(live=True, topic_string_keys=None),
            Exercise(live=False, topic_string_keys=['yes']),
            Exercise(live=False, topic_string_keys=['yes']),
            Exercise(live=True, topic_string_keys=['yes']),  # valid

        ]
        mock_get_all_use_cache_unsafe.side_effect = lambda *args, **kwargs: all_unsafe

        all_ = exercise.Exercise._get_all_use_cache_safe()
        self.assertEqual(
            len(list(all_)),
            1
        )

    def setUp(self):
        self.static_exercise = {
            'name': 'name',
            'pretty_display_name': 'pretty_display_name',
            'live': False,
            'description': 'description',
            'is_quiz_exercise': False,
            'large_screen_only': False,
        }

    @patch('content.internal.exercise.Exercise._get_all_use_cache_safe')
    def _test_user_cannot_assign_include_exam_to_get_all_exercise(
            self, live_only, include_exam, mock_get_all_safe
    ):
        mock_get_all_safe.return_value = [self.static_exercise]
        all_ = exercise.Exercise.get_all_use_cache(
            live_only=live_only,
            include_exam=include_exam
        )
        mock_get_all_safe.assert_called_once()
        self.assertEqual(
            len(all_),
            1
        )
        self.assertIsInstance(
            all_[0],
            exercise.Exercise
        )
        self.assertEqual(
            all_[0]._entity,
            self.static_exercise
        )

    @patch('content.internal.exercise.user_util.User.current')
    def test_user_cannot_assign_include_exam_to_get_all_exercise(
            self, mock_current_user
    ):
        class User:
            is_developer = False
        mock_current_user.side_effect = lambda: User()
        for live_only, include_exam in product([True, False], repeat=2):
            # https://lists.logilab.org/pipermail/python-projects/2013-January/683626.html
            # pylint: disable=no-value-for-parameter
            self._test_user_cannot_assign_include_exam_to_get_all_exercise(
                live_only,
                include_exam,
            )

    def assert_return_static_exercise_instance(self, all_exercise):
        self.assertEqual(
            len(all_exercise),
            1
        )
        self.assertIsInstance(
            all_exercise[0],
            exercise.Exercise
        )
        self.assertEqual(
            all_exercise[0]._entity,
            self.static_exercise
        )

    @patch('content.internal.exercise.Exercise._get_all_use_cache_unsafe')
    def _test_exclude_exam_without_live_only(self, mock_get_all_unsafe):
        mock_get_all_unsafe.return_value = [self.static_exercise]
        all_exercise = exercise.Exercise.get_all_use_cache(
            live_only=False,
            include_exam=False
        )
        mock_get_all_unsafe.assert_called_with(include_exam=False)
        self.assert_return_static_exercise_instance(all_exercise)

    @patch('content.internal.exercise.Exercise._get_all_use_cache_safe')
    def _test_exclude_exam_with_live_only(self, mock_get_all_safe):
        mock_get_all_safe.return_value = [self.static_exercise]
        all_exercise = exercise.Exercise.get_all_use_cache(
            live_only=True,
            include_exam=False
        )
        mock_get_all_safe.assert_called_once()
        self.assert_return_static_exercise_instance(all_exercise)

    @patch('content.internal.exercise.Exercise._get_all_use_cache_unsafe')
    def _test_exclude_exam(self, mock_get_all_unsafe):
        mock_get_all_unsafe.return_value = [self.static_exercise]
        all_exercise = exercise.Exercise.get_all_use_cache(include_exam=True)
        mock_get_all_unsafe.assert_called_with(include_exam=True)
        self.assert_return_static_exercise_instance(all_exercise)

    @patch('content.internal.exercise.user_util.User.current')
    def test_developer_can_assign_include_exam_to_get_all_exercise(
            self, mock_current_user
    ):
        class Developer:
            is_developer = True
        mock_current_user.return_value = Developer()

        # pylint: disable=no-value-for-parameter
        self._test_exclude_exam_without_live_only()
        # pylint: disable=no-value-for-parameter
        self._test_exclude_exam_with_live_only()
        # pylint: disable=no-value-for-parameter
        self._test_exclude_exam()


class TestExerciseUtil(unittest.TestCase):
    @patch('google.cloud.datastore.Query.fetch')
    def test_get_by_name(self, fetch_patch):
        # Test not found.
        fetch_patch.return_value = []
        ret = exercise.get_by_name('test_name')
        self.assertIsNone(ret)
        fetch_patch.assert_called_once()
        fetch_patch.reset_mock()

        # Test found.
        entity = mock_entity.get_exercise_entity()
        fetch_patch.return_value = [entity]
        ret = exercise.get_by_name(entity['name'])
        self.assertIs(ret, entity)
        fetch_patch.assert_called_once()

    @patch('google.cloud.datastore.Client.put')
    def test_add_new_exercise(self, put_patch):
        data = dict(mock_entity.get_exercise_entity())
        ret = exercise.add_new_exercise(data)
        put_patch.assert_called_once()
        self.assertEqual(put_patch.call_args[0], (ret,))
        self.assertEqual(ret.key.flat_path, ('Exercise',))
        self.assertEqual({k: ret[k] for k in data}, data)

    @patch('google.cloud.datastore.Query.fetch')
    def test_get_content_change(self, fetch_patch):
        version = mock_entity.get_topic_version_entity()
        content = mock_entity.get_exercise_entity()
        change = mock_entity.get_content_change_entity(version.key, content.key, ['description'])

        # Test not found.
        fetch_patch.return_value = []
        ret = exercise.get_content_change(content, version)
        self.assertIsNone(ret)
        fetch_patch.assert_called_once()
        fetch_patch.reset_mock()

        # Test found.
        fetch_patch.return_value = [change]
        ret = exercise.get_content_change(content, version)
        self.assertIs(ret, change)
        fetch_patch.assert_called_once()

    @patch('google.cloud.datastore.Client.put')
    @patch('google.cloud.datastore.Query.fetch')
    def test_add_content_change(self, fetch_patch, put_patch):
        # No existing VersionContentChange will be fetched.
        fetch_patch.return_value = []

        content = mock_entity.get_exercise_entity()
        version = mock_entity.get_topic_version_entity()

        # Test invalid change.
        data = {'description': None}
        exercise.add_content_change(content, version, data)
        put_patch.assert_not_called()

        # Test dummy change.
        data = {'description': content['description']}
        exercise.add_content_change(content, version, data)
        put_patch.assert_not_called()

        # Test add a change.
        data = {'description': content['description'] + 'x'}
        exercise.add_content_change(content, version, data)
        put_patch.assert_called_once()
        (change,) = put_patch.call_args[0]
        self.assertEqual(change.key.flat_path, version.key.flat_path + ('VersionContentChange',))
        self.assertEqual(change['version'], version.key)
        self.assertEqual(change['content'], content.key)
        self.assertEqual(change['content_changes'], pickle.dumps(data, protocol=2))
        self.assertEqual(change['last_edited_by'], None)
        self.assertEqual(type(change['updated_on']), datetime.datetime)

    def test_validate_name_format(self):
        self.assertRaisesRegex(
                exception.InvalidFormat, r"^代號不能空白$",
                exercise.validate_name_format, None)

        self.assertRaisesRegex(
                exception.InvalidFormat, r"^代號不能空白$",
                exercise.validate_name_format, '')

        # Expect not raises.
        name = ''.join(random.choices('abc123', k=500))
        exercise.validate_name_format(name)

        name = ''.join(random.choices('abc123', k=501))
        self.assertRaisesRegex(
                exception.InvalidFormat, r"^代號不能超過 500 個字, %s$" % re.escape(name),
                exercise.validate_name_format, name)

        name = 'test_name_with_' + random.choice('!@#$%^') + '_as_an_invalid_char'
        self.assertRaisesRegex(
                exception.InvalidFormat, r"^代號 %s 存在不合法字元$" % re.escape(name),
                exercise.validate_name_format, name)

    def test_validate_title_format(self):
        self.assertRaisesRegex(
                exception.InvalidFormat, r"^標題不能空白$",
                exercise.validate_title_format, None)

        self.assertRaisesRegex(
                exception.InvalidFormat, r"^標題不能空白$",
                exercise.validate_title_format, '')

        # Expect not raises.
        title = ''.join(random.choices('abc123', k=100))
        exercise.validate_title_format(title)

        title = ''.join(random.choices('abc123', k=101))
        self.assertRaisesRegex(
                exception.InvalidFormat, r"^標題不能超過 100 個字, %s$" % re.escape(title),
                exercise.validate_title_format, title)
