# -*- coding: utf-8 -*-
from typing import Optional

from .content_prototype import Content


class Url(Content):
    url: str
    title: str
    description: str
    is_hidden: bool

    _property_from_entity = ['url', 'title', 'description', 'is_hidden']

    @property
    def is_live(self):
        return True

    @property
    def is_teaching_material(self):
        return True

    @property
    def id(self):
        return self._entity.key.id

    def __init__(self, entity=None, parent_topic=None):
        super().__init__(entity, parent_topic)

    def info_to_topic_page(self):
        return {
            'url': self.url,
            'type': self.__class__.__name__,
            'title': self.title,
            'is_content': self.is_teaching_material,
            'id': self.id,
            'description': self.description,
        }

    def dump(self, props: Optional[list] = None) -> dict:
        return {
            'kind': self.__class__.__name__,
            'id': self.id,
            'title': self.title,
            'is_live': self.is_live,
        }
