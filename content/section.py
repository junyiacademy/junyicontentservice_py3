# -*- coding: utf-8 -*-
from .internal import topic
from .factory import get_content_factory

DEFAULT_TITLE = 'New Section'

def create_section(parent_id, section_id, title, standalone_title):
    section_title = title if title else DEFAULT_TITLE
    section_standalone_title = standalone_title if standalone_title else section_title
    return topic.create_topic(parent_id=parent_id,
                              topic_id=section_id,
                              title=section_title,
                              standalone_title=section_standalone_title,
                              is_section=True)

def read_section(*, section_id, version_id):
    section = topic.Section.from_id(section_id=section_id, version_id=version_id)
    section.content_factory = get_content_factory()
    return (not section.is_editable), section.dump()


def update_section(section_id, new_section_id, new_title, new_standalone_title):
    if new_section_id is None and new_title is None and new_standalone_title is None:
        return
    props = {}
    if new_section_id is not None:
        props['id'] = new_section_id
    if new_title is not None:
        props['title'] = new_title
    if new_standalone_title is not None:
        props['standalone_title'] = new_standalone_title
    topic.update_topic(False, section_id, **props)

def add_children(*, section_id, child_keys):
    section = topic.Section.from_id(section_id=section_id, version_id='edit')
    section.add_children(child_keys)


def delete_section(section_id):
    return topic.delete_topic(section_id)
