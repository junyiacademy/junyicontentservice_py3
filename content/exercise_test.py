# -*- coding: utf-8 -*-
import unittest
from unittest.mock import patch
import service_cache

from content.internal import exercise as internal_exercise
from content import exercise

from .exercise import create_exercise
from .exception import ExerciseCreateError


class TestExercise(unittest.TestCase):

    @patch('content.internal.exercise.create_exercise')
    @patch('content.internal.exercise.validate_consistency')
    def test_create_exercise_ok_in_validation_only(self, validate_consistency_patch, create_exercise_patch):
        create_exercise('mesfl4aa123', '【基礎】正三角形和等腰三角形', '從墳場把知識點撈出來上架', 'mesfl4a', 3, True)
        validate_consistency_patch.assert_called_once()
        create_exercise_patch.assert_not_called()

    @patch('content.internal.exercise.create_exercise')
    @patch('content.internal.exercise.validate_consistency')
    def test_create_exercise_error_in_validation_only(self, validate_consistency_patch, create_exercise_patch):
        validate_consistency_patch.side_effect = ExerciseCreateError('Go wrong!')
        errors = create_exercise('mesfl4aa123', '【基礎】正三角形和等腰三角形', '從墳場把知識點撈出來上架', 'mesfl4a', 3, True)
        self.assertEqual(errors, ['Go wrong!'])
        create_exercise_patch.assert_not_called()

    @patch('content.internal.exercise.create_exercise')
    @patch('content.internal.exercise.validate_consistency')
    def test_create_exercise_ok(self, validate_consistency_patch, create_exercise_patch):
        validate_consistency_patch.reset_mock()
        create_exercise_patch.reset_mock()
        create_exercise('mesfl4aa123', '【基礎】正三角形和等腰三角形', '從墳場把知識點撈出來上架', 'mesfl4a', 3, False)
        validate_consistency_patch.assert_not_called()
        create_exercise_patch.assert_called_once()

    @patch('content.internal.exercise.create_exercise')
    @patch('content.internal.exercise.validate_consistency')
    def test_create_exercise_error(self, validate_consistency_patch, create_exercise_patch):
        create_exercise_patch.side_effect = ExerciseCreateError('Go wrong!')
        errors = create_exercise('mesfl4aa123', '【基礎】正三角形和等腰三角形', '從墳場把知識點撈出來上架', 'mesfl4a', 3, False)
        validate_consistency_patch.assert_not_called()
        self.assertEqual(errors, ['Go wrong!'])

    @patch('content.exercise.exercise.Exercise.get_all_use_cache')
    def test_exercise_get_all_name_order_by_position(
            self, mock_get_all_use_cache
    ):
        '''
        Input: [
            {'name': 'first', 'pretty_display_name': 'first'},
            {'name': 'second', 'pretty_display_name': 'second'},
            {'name': 'third', 'pretty_display_name': 'third'},
            {'name': 'fourth', 'pretty_display_name': 'fourth'},
        ]
        Expect: [
            {'name': 'first', 'display_name': 'first'},
            {'name': 'second', 'display_name': 'second'},
            {'name': 'third', 'display_name': 'third'},
            {'name': 'fourth', 'display_name': 'fourth'},
        ]
        '''
        mock_get_all_use_cache.side_effect = lambda *args, **kwargs: list(map(
            internal_exercise.Exercise,
            [
                {'name': 'first', 'pretty_display_name': 'first'},
                {'name': 'second', 'pretty_display_name': 'second'},
                {'name': 'third', 'pretty_display_name': 'third'},
                {'name': 'fourth', 'pretty_display_name': 'fourth'},
            ]
        ))
        except_list = [
            {'name': 'first', 'display_name': 'first'},
            {'name': 'second', 'display_name': 'second'},
            {'name': 'third', 'display_name': 'third'},
            {'name': 'fourth', 'display_name': 'fourth'},
        ]
        service_cache.disable()
        all_name = exercise.exercise_get_all_name()
        self.assertEqual(
            all_name,
            except_list
        )
