# -*- coding: utf-8 -*-
import content.exception
import instance_cache
import service_cache
from .factory import get_content_factory
from .internal import internal
from .internal import topic


def get_version(version_id):
    return internal.get_version(version_id)


# if version_id is None, get default topic version, otherwise we should not cache the result
def get_topic_page(topic_id, include_hidden=False, version_id=None, current_user=None):
    leading_topic = topic.Topic.from_id(topic_id=topic_id, version_id=version_id, current_user=current_user)

    visible_content_selector = lambda c: include_hidden or c.is_live
    if not leading_topic or not visible_content_selector(leading_topic):
        raise content.exception.ContentNotExistsError('No Topic for topic_id [%s] in default version' % topic_id)

    leading_topic.content_factory = get_content_factory()
    return leading_topic.info_to_topic_page(leading_topic=True, visible_content_selector=visible_content_selector)


# if version_id is None, get default topic version, otherwise we should not cache the result
def get_topic_page_data(topic_id, include_hidden=False, version_id=None, current_user=None):
    leading_topic = topic.Topic.from_id(topic_id=topic_id, version_id=version_id, current_user=current_user)

    visible_content_selector = lambda c: include_hidden or c.is_live
    if not leading_topic or not visible_content_selector(leading_topic):
        raise content.exception.ContentNotExistsError('No Topic for topic_id [%s] in default version' % topic_id)

    leading_topic.content_factory = get_content_factory()
    return leading_topic.info_to_topic_page_v1(True, visible_content_selector)


@service_cache.cache_with_key_fxn(
    lambda version: f"get_topic_tree_data_v{version['number']}",
    persist_across_app_versions=True)
def get_topic_tree_data(version):
    root_topic = topic.get_root(version)
    return root_topic.subtree()


@service_cache.cache_with_key_fxn(
    lambda version: f"get_topic_tree_data_with_publisher_v{version['number']}",
    persist_across_app_versions=True)
def get_topic_tree_data_with_publisher(version):
    data = get_topic_tree_data(version)
    _add_publisher_topics_for_course_compare(data)
    return data


def _add_publisher_topics_for_course_compare(data):
    PUBLISHER_LIST = [
        ('publisher_jun', '均一版'),
        ('publisher_nan', '南一版'),
        ('publisher_kan', '康軒版'),
        ('publisher_han', '翰林版')
    ]

    def find_topic(topics, id):
        for topic in topics:
            if topic['id'] == id:
                return topic
        return None

    def create_pseudo_topic(id, title):
        return {
            'id': id,
            'title': title,
            'childTopics': [],
            'tags': ['has-pre-exam', 'has-post-exam', 'has-topic-exam'],
            'isContentTopic': False,
            'key': '',
        }

    # find `course-compare`, do nothing if not found
    course_compare = find_topic(data['childTopics'], 'course-compare')
    if not course_compare:
        return

    # get original child topics
    child_topics = course_compare['childTopics']

    # create pseudo publisher topics and append to `course-compare`
    course_compare['childTopics'] = []
    publisher_topics = {}
    for (publisher_id, publisher_title) in PUBLISHER_LIST:
        publisher_topic = create_pseudo_topic(publisher_id, publisher_title)
        publisher_topics[publisher_id] = publisher_topic
        course_compare['childTopics'].append(publisher_topic)

    # copy original children into corresponding publisher topic
    for child in child_topics:
        for (publisher_id, _) in PUBLISHER_LIST:
            if publisher_id in child['tags']:
                publisher_topic = publisher_topics[publisher_id]
                publisher_topic['childTopics'].append(child)


@instance_cache.cache_with_key_fxn(
    lambda topic_id, version:
    f"export_topic_tree_data_v{version['number']}_{topic_id}"
)
def export_topic_tree_data(topic_id, version):
    entity = topic._get_by_id(topic_id, version)
    if entity is None:
        raise content.exception.ContentNotExistsError('No Topic for topic_id [%s] in default version' % topic_id)
    t = topic.Topic(entity=entity)
    t.content_factory = get_content_factory()
    return t.export()


def validate_topic_required_info_by_edu_sheet(topic_id, title, version_id):
    errors = []

    try:
        topic.validate_id_format(topic_id)
    except content.exception.InvalidFormat as e:
        errors.append(str(e))

    try:
        topic.validate_title(title)
    except content.exception.InvalidFormat as e:
        errors.append(str(e))

    if version_id != 'edit':
        errors.append(u"不允許編輯此版本 [%s]" % version_id)

    try:
        internal.get_edit_version()
    except internal.InvalidContentTree:
        errors.append(u"嚴重錯誤：edit version 不存在")

    return errors


def update_topic(topic_id, title, description, validation_only, version_id):
    errors = validate_topic_required_info_by_edu_sheet(topic_id, title, version_id)

    try:
        updated_topic = topic.update_topic(validation_only=validation_only,
                                           topic_id=topic_id,
                                           title=title,
                                           standalone_title=title,
                                           description=description)
    except content.exception.ContentNotExistsError as e:
        errors.append(str(e))
        updated_topic = None

    return errors, updated_topic


def create_topic(topic_id, title, description, parent_id, version_id):
    errors = []
    try:
        created_topic = topic.create_topic(title=title,
                                           parent_id=parent_id,
                                           topic_id=topic_id,
                                           standalone_title=title,
                                           description=description,
                                           version_id=version_id)
    except (content.exception.TopicExistsError,
            content.exception.ContentNotExistsError,
            content.exception.InvalidParent,
            content.exception.InvalidVersion,) as e:
        errors.append(str(e))
        created_topic = None

    return errors, created_topic


def get_exercise_topics(version_id):
    version = internal.get_version(version_id)
    topics = topic.get_filled_content_topics(types=['Exercise'],
                                             version=version)
    # Topics in ignored_topics will not show up on the knowledge map,
    # have topic exercise badges created for them, etc.
    ignored_topics = [
        "New and Noteworthy",
    ]

    # Filter out New and Noteworthy special-case topic. It might
    # have exercises, but we don't want it to own a badge.
    topics = [t for t in topics if t.title not in ignored_topics and len(t.children) > 0]

    # Remove non-live exercises
    for t in topics:
        t.children = [exercise for exercise in t.children if exercise['live']]

    # Transform TopicVersion to JSON
    version_json = {
        'entity_key_id': version.id,
        'kind': version.kind,
        'number': version['number'],
        'default': version['default'],
        'edit': version['edit'],
    }

    # Filter down to only topics that have live exercises
    return [t.topic_to_json(version_json) for t in topics if len(t.children) > 0]
