'''
The config classes for Flask APP
Reference: https://flask.palletsprojects.com/en/1.1.x/config/#
'''
import os


class Config:
    """
    The base config
    """
    YOUTUBE_SYNC_API_KEY = os.environ.get("YOUTUBE_SYNC_API_KEY", None)
    DEFAULT_SERVICE_SERVER_URL = os.environ.get(
        "DEFAULT_SERVICE_SERVER_URL",
        'http://localhost:8080'
    )

    @property
    def IN_LIVE(self):
        return '://junyiacademy.' in self.DEFAULT_SERVICE_SERVER_URL

    @property
    def IN_TEST(self):
        return '://junyiacademytest1.' in self.DEFAULT_SERVICE_SERVER_URL

    @property
    def IN_LOCAL(self):
        return '://localhost' in self.DEFAULT_SERVICE_SERVER_URL


class TestConfig(Config):
    """
    Class for testing, you can modify your config variable
    in this class for your tests.
    """
    pass
