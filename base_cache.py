import functools
import pickle

# 100000 is max for memory cache
MAX_SIZE_OF_CACHE = 1000 * 1000
# max for memory cache - ChunkedResult overhead
MAX_SIZE_OF_CACHE_CHUNKS = MAX_SIZE_OF_CACHE - 100


class Debug:
    DBG = False

    @staticmethod
    def print(param):
        if Debug.DBG:
            print(param)

    @staticmethod
    def trim(a):
        s = str(a)
        if len(s) > 64:
            s = s[:64] + '...'
        return s

    @staticmethod
    def debug(target):
        @functools.wraps(target)
        def wrapper(*args, **kwargs):
            args_ = [Debug.trim(a) for a in args[1:]]
            Debug.print('    >>>> [%s] called with args=%s, kwargs=%s'
                        % (target.__qualname__, args_, kwargs))
            return target(*args, **kwargs)

        return wrapper


def get_namespaced_key(key, namespace):
    if namespace is None:
        return key
    else:
        return f'{namespace}:{key}'


class BaseCache:
    @classmethod
    @Debug.debug
    def get(cls, key, namespace=None):
        value = cls._get_value(get_namespaced_key(key, namespace))
        if value is None:
            return None
        else:
            return pickle.loads(value)

    @classmethod
    @Debug.debug
    def get_multi(cls, keys, namespace=None):
        values = {}
        for key in keys:
            value = cls.get(key, namespace=namespace)
            if value is not None:
                values[key] = value
        return values

    @classmethod
    @Debug.debug
    def set(cls, key, value, time=0, namespace=None):
        value = pickle.dumps(value)
        if len(value) > MAX_SIZE_OF_CACHE:
            raise ValueError("Values may not be more than %s: %s" % (MAX_SIZE_OF_CACHE, len(value)))
        cls._set_value(get_namespaced_key(key, namespace), value, time)
        return True

    @classmethod
    @Debug.debug
    def set_multi(cls,
                  mapping,
                  time=0,
                  namespace=None):
        for key, value in mapping.items():
            cls.set(key, value, time=time, namespace=namespace)

    @classmethod
    def delete(cls, key, namespace=None):
        cls._delete_value(get_namespaced_key(key, namespace))

    @classmethod
    def delete_multi(cls, keys, namespace):
        for key in keys:
            cls.delete(key, namespace=namespace)

    @classmethod
    def flush(cls):
        raise NotImplementedError

    @classmethod
    def _get_value(cls, key):
        raise NotImplementedError

    @classmethod
    def _set_value(cls, key, value, time):
        raise NotImplementedError

    @classmethod
    def _delete_value(cls, key):
        raise NotImplementedError
