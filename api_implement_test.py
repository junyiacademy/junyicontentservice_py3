# -*- coding: utf-8 -*-
import unittest
import unittest.mock as mock
from unittest.mock import patch

import api_implement
import content.video


class TestGetTopicPageJson(unittest.TestCase):
    # TODO: topic_id = None
    def test_no_default_version(self):
        pass

    @patch('google.cloud.datastore.Client.put')
    def test_validate_or_update_topic_by_edu_sheet_ok(self, put_patch):
        topic_id = 'math'
        # 測試驗證
        ret = api_implement.validate_or_update_topic_by_edu_sheet(topic_id, 'title', 'description', True)
        self.assertEqual(ret['topic_id'], topic_id)
        self.assertEqual(len(ret['errors']), 0)
        self.assertFalse(put_patch.called)
        # 測試修改
        ret = api_implement.validate_or_update_topic_by_edu_sheet(topic_id, 'title', 'description', False)
        self.assertEqual(ret['topic_id'], topic_id)
        self.assertEqual(len(ret['errors']), 0)
        self.assertTrue(put_patch.called)

    def test_validate_or_update_topic_by_edu_sheet_fail(self):
        # 測試驗證時發生錯誤 - id 不存在
        topic_id = 'x'
        ret = api_implement.validate_or_update_topic_by_edu_sheet(topic_id, 'title', 'description', False)
        self.assertEqual(ret['topic_id'], topic_id)
        self.assertEqual(len(ret['errors']), 1)
        self.assertEqual(ret['errors'][0], '這個代號的資料夾不存在 x')

    @patch('google.cloud.datastore.Client.put')
    def test_validate_or_create_topic_by_edu_sheet_ok(self, put_patch):
        topic_id = 'new_topic'
        # 測試驗證
        ret = api_implement.validate_or_create_topic_by_edu_sheet(topic_id, 'title', 'description', 'math', True)
        self.assertEqual(ret['topic_id'], topic_id)
        self.assertEqual(len(ret['errors']), 0)
        self.assertFalse(put_patch.called)
        # 測試建立
        ret = api_implement.validate_or_create_topic_by_edu_sheet(topic_id, 'title', 'description', 'math', False)
        self.assertEqual(ret['topic_id'], topic_id)
        self.assertEqual(len(ret['errors']), 0)
        self.assertTrue(put_patch.called)

    def test_validate_or_create_topic_by_edu_sheet_fail(self):
        # 測試驗證時發生錯誤 - id 空白
        topic_id = ''
        ret = api_implement.validate_or_create_topic_by_edu_sheet(topic_id, 'title', 'description', 'math', False)
        self.assertEqual(ret['topic_id'], topic_id)
        self.assertEqual(len(ret['errors']), 1)
        # 測試建立時發生錯誤 - 不存在 parent
        topic_id = 'new_topic'
        ret = api_implement.validate_or_create_topic_by_edu_sheet(topic_id, 'title', 'description', 'x', False)
        self.assertEqual(len(ret['errors']), 1)
        # 測試建立時發生錯誤 - 已存在 id
        topic_id = 'math'
        ret = api_implement.validate_or_create_topic_by_edu_sheet(topic_id, 'title', 'description', 'math', False)
        self.assertEqual(len(ret['errors']), 1)

    def test_validate_or_create_topic_by_edu_sheet_fail_version(self):
        # 測試在 default version 變動
        topic_id = 'new_topic'
        ret = api_implement.validate_or_create_topic_by_edu_sheet(topic_id, 'title', 'description', 'math', True, 'default')
        self.assertEqual(ret['topic_id'], topic_id)
        self.assertEqual(len(ret['errors']), 1)
        self.assertEqual(ret['errors'][0], '目前禁止於 edit 以外的版本建立 Topic')
        ret = api_implement.validate_or_update_topic_by_edu_sheet(topic_id, 'title', 'description', True, 'default')
        self.assertEqual(ret['topic_id'], topic_id)
        self.assertEqual(len(ret['errors']), 1)
        self.assertEqual(ret['errors'][0], '目前禁止於 edit 以外的版本修改 Topic')


class TestEduSheetVideoAPI(unittest.TestCase):
    @patch('pickle.dumps')
    @patch('google.cloud.datastore.Client.put')
    @patch('content.internal.youtube_sync.youtube_get_video_data_dict')
    def test_validate_or_create_video_by_edu_sheet_ok(self, youtube_get_video_data_dict_patch, put_patch, pickle_dumps_patch):
        ret = api_implement.validate_or_create_video_by_edu_sheet(
            'readable_id', 'title', 'description', 'math', 3, False)
        self.assertEqual(ret['readable_id'], 'readable_id')
        self.assertListEqual(ret['errors'], [])
        put_patch.assert_called()

    @patch('google.cloud.datastore.Client.put')
    def test_validate_or_create_video_by_edu_sheet_invalid_version(self, put_patch):
        ret = api_implement.validate_or_create_video_by_edu_sheet(
            'readable_id', 'title', 'description', 'math', 3, False, 'non_edit')
        self.assertListEqual(ret['errors'], ['不允許編輯此版本 [non_edit]'])
        put_patch.assert_not_called()

    @patch('google.cloud.datastore.Client.put')
    def test_validate_or_update_video_by_edu_sheet_invalid_version(self, put_patch):
        ret = api_implement.validate_or_update_video_by_edu_sheet(
            'readable_id', 'title', 'description', 3, False, 'non_edit')
        self.assertListEqual(ret['errors'], ['不允許編輯此版本 [non_edit]'])
        put_patch.assert_not_called()


class TestVideoGetAll(unittest.TestCase):

    def setUp(self):
        super(TestVideoGetAll, self).setUp()
        self.addCleanup(patch.stopall)
        self.get_all_videos_mock = patch.object(
            content.video, 'get_all_videos', autospec=True).start()

    def test_video_get_all_impl(self):
        expected = [
            dict(display_name='video 1', readable_id='id-a'),
            dict(display_name='video 2', readable_id='id-b'),
        ]
        self.get_all_videos_mock.return_value = iter([
            mock.MagicMock(readable_id='id-a', title='video 1'),
            mock.MagicMock(readable_id='id-b', title='video 2'),
        ])
        actual = api_implement.video_get_all_impl()
        self.assertEqual(expected, actual)

    def test_video_get_all_impl_missing_id_attr_error(self):
        self.get_all_videos_mock.return_value = iter([
            mock.MagicMock(title='video 1', spec_set=['title']),
        ])
        self.assertRaises(AttributeError, api_implement.video_get_all_impl)

    def test_video_get_all_impl_missing_title_attr_error(self):
        self.get_all_videos_mock.return_value = iter([
            mock.MagicMock(readable_id='id-a', spec_set=['readable_id']),
        ])
        self.assertRaises(AttributeError, api_implement.video_get_all_impl)


class TestTopicExport(unittest.TestCase):
    @patch('content.topic.get_version')
    @patch('content.topic.export_topic_tree_data')
    def test_export_topic_tree_data(self,
                                    export_topic_tree_data_patch,
                                    get_version_patch):
        topic_id = 'fake_id'
        version_id = 'fake_id'
        version = mock.Mock()
        get_version_patch.return_value = version
        api_implement.export_topic_tree_data(topic_id, version_id)
        get_version_patch.assert_called_once_with(version_id)
        export_topic_tree_data_patch.assert_called_once_with(topic_id, version)

    @patch('api_implement._format_content')
    def test_format_topic(self, _format_content_patch):
        _format_content_patch.return_value = 'mock\n'
        data = {
            'id': 'test_id', 'title': 'test_title', 'is_live': True,
            'child_topics': [{
                'id': 'test_child_id', 'title': 'test_child_title', 'is_live': True
            }],
            'child_contents': [mock.Mock()]
        }
        ret = api_implement._format_topic(data)
        self.assertIn(data['id'], ret)
        self.assertIn(data['title'], ret)
        self.assertIn(data['child_topics'][0]['id'], ret)
        self.assertIn(data['child_topics'][0]['title'], ret)
        _format_content_patch.assert_called_once_with(data['child_contents'][0])

    def test_format_content(self):
        data = {'kind': 'test_kind', 'id': 'test_id', 'title': 'test_title', 'is_live': True}
        ret = api_implement._format_content(data)
        self.assertIn(data['kind'], ret)
        self.assertIn(data['id'], ret)

    def test_format_item(self):
        data = {'id': 'test_id', 'title': 'test_title'}
        # test hidden
        data['is_live'] = False
        ret = api_implement._format_item(data)
        self.assertIn(data['id'], ret)
        self.assertIn(data['title'], ret)
        self.assertIn('hidden', ret)
        # test not hidden
        data['is_live'] = True
        ret = api_implement._format_item(data)
        self.assertNotIn('hidden', ret)
